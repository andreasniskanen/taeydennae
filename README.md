taeydennae : SAT-based Reasoner for Incomplete and Control Argumentation Frameworks
===================================================================================

Version    : 2020.08.11

Author     : [Andreas Niskanen](mailto:andreas.niskanen@helsinki.fi), University of Helsinki

Installation
------------

To compile the program:
```
make
```

To remove all object files:
```
make clean
```

Usage
-----

```
./bin/taeydennae [options] -p <task> -f <file> -a <query>

  <task>      computational task in the form {PCA|NCA|PSA|NSA|PEXSA|NEXSA|CC|SC}-{AD|NEAD|CO|PR|ST|GR}
  <file>      input incomplete or control AF in apx format extended with ?arg, ?att, satt, and carg predicates
  <query>     query or target argument(s) as a comma-separated list

Options:
  --formats   Prints available file formats.
  --problems  Prints available computational tasks.
  --version   Prints version and author information.
  --qdimacs   QDIMACS output (for controllability problems).
```

This program supports all acceptance tasks over incomplete and control AFs:

| abbr. | computational problem                        |
|-------|----------------------------------------------|
| PCA   | possible credulous acceptance                |
| NCA   | necessary credulous acceptance               |
| PSA   | possible skeptical acceptance                |
| NSA   | necessary skeptical acceptance               |
| PEXSA | possible existence and skeptical acceptance  |
| NEXSA | necessary existence and skeptical acceptance |
| CC    | credulous controllability                    |
| SC    | skeptical controllability                    |

under the following AF semantics:

| abbr. | semantics            |
|-------|----------------------|
| AD    | admissible           |
| NEAD  | non-empty admissible |
| CO    | complete             |
| PR    | preferred            |
| ST    | stable               |
| GR    | grounded             |

The task and semantics is provided as input using `-p <task>`.

The input file, given via `-f <file>` must be an incomplete or a control AF in apx format with the following predicates.

```
arg(x).   # x is a definite argument
?arg(x)   # x is an uncertain argument
carg(x).  # x is a control argument
att(x,y)  # (x,y) is a definite attack
?att(x,y) # (x,y) is an uncertain attack
satt(x,y) # (x,y) and (y,x) are uncertain attacks but one exists
```

The SAT solver used in this software, [Glucose](https://www.labri.fr/perso/lsimon/glucose/) (version 4.1), is included in this release.


Please direct any questions, comments, bug reports etc. directly to [the author](mailto:andreas.niskanen@helsinki.fi).


Finally, please use the following references for `taeydennae`:

Deciding Acceptance in Incomplete Argumentation Frameworks.
Andreas Niskanen, Daniel Neugebauer, Matti Järvisalo, Jörg Rothe.
Proceedings of AAAI 2020.

Controllability of Control Argumentation Frameworks.
Andreas Niskanen, Daniel Neugebauer, Matti Järvisalo.
Proceedings of IJCAI-PRICAI 2020.