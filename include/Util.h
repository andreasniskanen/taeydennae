/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef UTIL_H
#define UTIL_H

#include "AF.h"

std::vector<int> completion_assumptions(const AF & af, const std::vector<uint8_t> & assignment, int additional_assumption);
std::vector<int> control_assumptions(const AF & af, const std::vector<uint8_t> & assignment, int additional_assumption);

std::vector<int> trivial_completion_refinement(const AF & af, const std::vector<uint8_t> & assignment);
std::vector<int> optimized_completion_refinement(const AF & af, const std::vector<uint8_t> & assignment, semantics sem);

std::vector<int> trivial_control_refinement(const AF & af, const std::vector<uint8_t> & assignment);
std::vector<int> optimized_control_refinement(const AF & af, const std::vector<uint8_t> & assignment, semantics sem);
std::vector<int> core_control_refinement(const AF & af, const std::vector<int> & conflict);

void print_completion(const AF & af, const std::vector<uint8_t> & assignment);
void print_control_configuration(const AF & af, const std::vector<uint8_t> & assignment);

void print_clause(const std::vector<int> & clause);
void print_clause_with_selector(const std::vector<int> & clause, const int selector_var);

#endif