/*!
 * Copyright (c) <2019> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"
#include "Util.h"
#include "NecExSkeptAcceptance.h"
#include <iostream>

using namespace std;

namespace NecExSkeptAcceptance {

bool admissible_ne(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_admissible(af, solver);
	vector<int> ne_clause(af.args+1);
	ne_clause[0] = -af.selector_var;
	for (uint32_t i = 0; i < af.args; i++) {
		ne_clause[i+1] = af.arg_accepted_var[i];
	}
	solver.add_clause(ne_clause);
	vector<int> query_arg_rejected = { af.selector_var, -af.target_var };
	if (solver.solve(query_arg_rejected)) return false;
	vector<int> query_arg_accepted = { af.selector_var, af.target_var };
	int count = 0;
	while (solver.solve(query_arg_accepted)) {
		count += 1;
#if defined(REFINEMENT)
		vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, NEAD);
#else
		vector<int> blocking_clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(blocking_clause);		
	}
	vector<int> select = { -af.selector_var };
	cout << "c Iterations: " << count << "\n";
	return !solver.solve(select);
}

bool stable(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_stable_with_selector(af, solver);
	vector<int> query_arg_rejected = { af.selector_var, -af.target_var };
	if (solver.solve(query_arg_rejected)) return false;
	vector<int> query_arg_accepted = { af.selector_var, af.target_var };
	int count = 0;
	while (solver.solve(query_arg_accepted)) {
		count += 1;
#if defined(REFINEMENT)
		vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, ST);
#else
		vector<int> blocking_clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(blocking_clause);		
	}
	vector<int> select = { -af.selector_var };
	cout << "c Iterations: " << count << "\n";
	return !solver.solve(select);
}

}