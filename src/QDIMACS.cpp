/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "QDIMACS.h"
#include "Util.h"

#include <iostream>

using namespace std;

namespace QDIMACS {

void arg_exists_and_accepted_clauses(const AF & af, vector<vector<int>> & clauses)
{
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.arg_exists_and_accepted_var[i], af.arg_accepted_var[i] };
			clauses.push_back(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.arg_exists_and_accepted_var[i], af.arg_exists_var[i] };
			clauses.push_back(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_exists_and_accepted_var[i], -af.arg_accepted_var[i], -af.arg_exists_var[i] };
			clauses.push_back(clause);
		}
	}
}

void source_accepted_clauses(const AF & af, vector<vector<int>> & clauses)
{
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { -af.source_accepted_var.at(af.uncertain_atts[i]), af.att_exists_var.at(af.uncertain_atts[i]) };
		clauses.push_back(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { -af.source_accepted_var.at(af.uncertain_atts[i]), af.arg_accepted_var[af.uncertain_atts[i].first] };
		clauses.push_back(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { -af.source_accepted_var.at(af.uncertain_atts[i]), af.arg_exists_var[af.uncertain_atts[i].first] };
			clauses.push_back(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (!af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { af.source_accepted_var.at(af.uncertain_atts[i]), -af.arg_accepted_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			clauses.push_back(clause);
		} else {
			vector<int> clause = { af.source_accepted_var.at(af.uncertain_atts[i]), -af.arg_accepted_var[af.uncertain_atts[i].first], -af.arg_exists_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			clauses.push_back(clause);
		}
	}
}

void arg_rejected_clauses(const AF & af, vector<vector<int>> & clauses)
{
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.attackers[i][j]]) {
				vector<int> clause = { af.arg_rejected_var[i], -af.arg_accepted_var[af.attackers[i][j]] };
				clauses.push_back(clause);
			} else {
				vector<int> clause = { af.arg_rejected_var[i], -af.arg_exists_and_accepted_var[af.attackers[i][j]] };
				clauses.push_back(clause);
			}
		}
		for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
			vector<int> clause = { af.arg_rejected_var[i], -af.source_accepted_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
			clauses.push_back(clause);
		}
		vector<int> clause(af.attackers[i].size() + af.uncertain_attackers[i].size() + 1);
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.attackers[i][j]]) {
				clause[j] = af.arg_accepted_var[af.attackers[i][j]];
			} else {
				clause[j] = af.arg_exists_and_accepted_var[af.attackers[i][j]];
			}
		}
		for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
			clause[j+af.attackers[i].size()] = af.source_accepted_var.at(make_pair(af.uncertain_attackers[i][j], i));
		}
		clause[af.attackers[i].size() + af.uncertain_attackers[i].size()] = -af.arg_rejected_var[i];
		clauses.push_back(clause);
	}
}

void arg_exists_implies_rejected_clauses(const AF & af, vector<vector<int>> & clauses)
{
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_exists_implies_rejected_var[i], -af.arg_rejected_var[i] };
			clauses.push_back(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_exists_implies_rejected_var[i], af.arg_exists_var[i] };
			clauses.push_back(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.arg_exists_implies_rejected_var[i], af.arg_rejected_var[i], -af.arg_exists_var[i] };
			clauses.push_back(clause);
		}
	}
}

void source_rejected_clauses(const AF & af, vector<vector<int>> & clauses)
{
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { af.source_rejected_var.at(af.uncertain_atts[i]), -af.arg_rejected_var[af.uncertain_atts[i].first] };
		clauses.push_back(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { af.source_rejected_var.at(af.uncertain_atts[i]), af.att_exists_var.at(af.uncertain_atts[i]) };
		clauses.push_back(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { af.source_rejected_var.at(af.uncertain_atts[i]), af.arg_exists_var[af.uncertain_atts[i].first] };
			clauses.push_back(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (!af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { -af.source_rejected_var.at(af.uncertain_atts[i]), af.arg_rejected_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			clauses.push_back(clause);
		} else {
			vector<int> clause = { -af.source_rejected_var.at(af.uncertain_atts[i]), af.arg_rejected_var[af.uncertain_atts[i].first], -af.arg_exists_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			clauses.push_back(clause);
		}
	}
}

void target_clauses(const AF & af, vector<vector<int>> & clauses) {
	if (af.target_args.size() == 1) return;
	for (uint32_t i = 0; i < af.target_args.size(); i++) {
		vector<int> clause = { -af.target_var, af.arg_accepted_var[af.target_args[i]] };
		clauses.push_back(clause);
	}
	vector<int> clause = { af.target_var };
	for (uint32_t i = 0; i < af.target_args.size(); i++) {
		clause.push_back(-af.arg_accepted_var[af.target_args[i]]);
	}
	clauses.push_back(clause);
}

void additional_clauses(const AF & af, vector<vector<int>> & clauses) {
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		vector<int> clause = { af.arg_exists_var[af.uncertain_args[i]], -af.arg_accepted_var[af.uncertain_args[i]] };
		clauses.push_back(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		for (uint32_t j = 0; j < af.uncertain_atts.size(); j++) {
			if (af.uncertain_atts[j].first == af.uncertain_args[i] || af.uncertain_atts[j].second == af.uncertain_args[i]) {
				vector<int> clause = { af.arg_exists_var[af.uncertain_args[i]], -af.att_exists_var.at(af.uncertain_atts[j]) };
				clauses.push_back(clause);
			}
		}
	}   
}

void symmetric_clauses(const AF & af, vector<vector<int>> & clauses)
{
	for (uint32_t i = 0; i < af.symmetric_atts.size(); i++) {
		if (!af.is_uncertain_arg[af.symmetric_atts[i].first] && !af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			vector<int> clause = { af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			clauses.push_back(clause);
		} else if (af.is_uncertain_arg[af.symmetric_atts[i].first] && !af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			vector<int> clause = { -af.arg_exists_var[af.symmetric_atts[i].first], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			clauses.push_back(clause);
		} else if (!af.is_uncertain_arg[af.symmetric_atts[i].first] && af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			vector<int> clause = { -af.arg_exists_var[af.symmetric_atts[i].second], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			clauses.push_back(clause);
		} else {
			vector<int> clause = { -af.arg_exists_var[af.symmetric_atts[i].first], -af.arg_exists_var[af.symmetric_atts[i].second], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			clauses.push_back(clause);			
		}
	}
}

void conflict_free(const AF & af, vector<vector<int>> & clauses)
{
	target_clauses(af, clauses);
	additional_clauses(af, clauses);
	symmetric_clauses(af, clauses);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			for (uint32_t j = 0;  j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_accepted_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				} else {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_exists_var[af.attackers[i][j]], -af.arg_accepted_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				}
			}
			for (uint32_t j = 0;  j < af.uncertain_attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.uncertain_attackers[i][j]]) {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					clauses.push_back(clause);
				} else {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_exists_var[af.uncertain_attackers[i][j]], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					clauses.push_back(clause);
				}
			}
		} else {
			for (uint32_t j = 0;  j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_accepted_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				} else {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_exists_var[af.attackers[i][j]], -af.arg_accepted_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				}
			}
			for (uint32_t j = 0;  j < af.uncertain_attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.uncertain_attackers[i][j]]) {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					clauses.push_back(clause);
				} else {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_exists_var[af.uncertain_attackers[i][j]], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					clauses.push_back(clause);
				}
			}
		}
	}
}

void admissible(const AF & af, vector<vector<int>> & clauses)
{
	conflict_free(af, clauses);
	arg_exists_and_accepted_clauses(af, clauses);
	source_accepted_clauses(af, clauses);
	arg_rejected_clauses(af, clauses);
	arg_exists_implies_rejected_clauses(af, clauses);
	source_rejected_clauses(af, clauses);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_accepted_var[i], af.arg_rejected_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				} else {
					vector<int> clause = { -af.arg_accepted_var[i], af.arg_exists_implies_rejected_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				vector<int> clause = { -af.arg_accepted_var[i], af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
				clauses.push_back(clause);
			}
		} else {
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_exists_var[i], -af.arg_accepted_var[i], af.arg_rejected_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				} else {
					vector<int> clause = { -af.arg_exists_var[i], -af.arg_accepted_var[i], af.arg_exists_implies_rejected_var[af.attackers[i][j]] };
					clauses.push_back(clause);
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				vector<int> clause = { -af.arg_exists_var[i], -af.arg_accepted_var[i], af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
				clauses.push_back(clause);
			}
		}
	}
}

void complete(const AF & af, vector<vector<int>> & clauses)
{
	admissible(af, clauses);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			vector<int> clause(af.attackers[i].size()+af.uncertain_attackers[i].size()+1);
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					clause[j] = -af.arg_rejected_var[af.attackers[i][j]];
				} else {
					clause[j] = -af.arg_exists_implies_rejected_var[af.attackers[i][j]];
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				clause[j+af.attackers[i].size()] = -af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i));
			}
			clause[af.attackers[i].size() + af.uncertain_attackers[i].size()] = af.arg_accepted_var[i];
			clauses.push_back(clause);
		} else {
			vector<int> clause(af.attackers[i].size()+af.uncertain_attackers[i].size()+2);
			clause[0] = -af.arg_exists_var[i];
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					clause[j+1] = -af.arg_rejected_var[af.attackers[i][j]];
				} else {
					clause[j+1] = -af.arg_exists_implies_rejected_var[af.attackers[i][j]];
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				clause[j+af.attackers[i].size()+1] = -af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i));
			}
			clause[af.attackers[i].size() + af.uncertain_attackers[i].size()+1] = af.arg_accepted_var[i];
			clauses.push_back(clause);
		}
	}
}

void stable(const AF & af, vector<vector<int>> & clauses)
{
	conflict_free(af, clauses);
	arg_exists_and_accepted_clauses(af, clauses);
	source_accepted_clauses(af, clauses);
#if defined(STB_AUX)
	arg_rejected_clauses(af, clauses);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_accepted_var[i], af.arg_rejected_var[i] };
			clauses.push_back(clause);
		} else {
			vector<int> clause = { -af.arg_exists_var[i], af.arg_accepted_var[i], af.arg_rejected_var[i] };
			clauses.push_back(clause);
		}
	}
#else
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			vector<int> clause(af.attackers[i].size() + af.uncertain_attackers[i].size() + 1);
			clause[0] = af.arg_accepted_var[i];
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					clause[j+1] = af.arg_accepted_var[af.attackers[i][j]];
				} else {
					clause[j+1] = af.arg_exists_and_accepted_var[af.attackers[i][j]];
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				clause[j+af.attackers[i].size()+1] = af.source_accepted_var.at(make_pair(af.uncertain_attackers[i][j], i));
			}
			clauses.push_back(clause);
		} else {
			vector<int> clause(af.attackers[i].size() + af.uncertain_attackers[i].size() + 2);
			clause[0] = -af.arg_exists_var[i];
			clause[1] = af.arg_accepted_var[i];
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					clause[j+2] = af.arg_accepted_var[af.attackers[i][j]];
				} else {
					clause[j+2] = af.arg_exists_and_accepted_var[af.attackers[i][j]];
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				clause[j+af.attackers[i].size()+2] = af.source_accepted_var.at(make_pair(af.uncertain_attackers[i][j], i));
			}
			clauses.push_back(clause);
		}
	}
#endif
}

int invalid_completion(const AF & af, vector<vector<int>> & clauses)
{
	int count = af.count;
	unordered_map<pair<uint32_t,uint32_t>,int> source_exists_and_attack_does_not_var;
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			source_exists_and_attack_does_not_var[af.uncertain_atts[i]] = ++count;
		}
	}
	unordered_map<pair<uint32_t,uint32_t>,int> target_exists_and_attack_does_not_var;
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].second]) {
			target_exists_and_attack_does_not_var[af.uncertain_atts[i]] = ++count;
		}
	}
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		for (uint32_t j = 0; j < af.uncertain_atts.size(); j++) {
			if (af.uncertain_atts[j].first == af.uncertain_args[i]) {
				clauses.push_back({ -source_exists_and_attack_does_not_var[af.uncertain_atts[j]], -af.arg_exists_var[af.uncertain_args[i]] });
				clauses.push_back({ -source_exists_and_attack_does_not_var[af.uncertain_atts[j]], af.att_exists_var.at(af.uncertain_atts[j]) });
				clauses.push_back({ source_exists_and_attack_does_not_var[af.uncertain_atts[j]], af.arg_exists_var[af.uncertain_args[i]], -af.att_exists_var.at(af.uncertain_atts[j]) });
			}
			if (af.uncertain_atts[j].second == af.uncertain_args[i]) {
				clauses.push_back({ -target_exists_and_attack_does_not_var[af.uncertain_atts[j]], -af.arg_exists_var[af.uncertain_args[i]] });
				clauses.push_back({ -target_exists_and_attack_does_not_var[af.uncertain_atts[j]], af.att_exists_var.at(af.uncertain_atts[j]) });
				clauses.push_back({ target_exists_and_attack_does_not_var[af.uncertain_atts[j]], af.arg_exists_var[af.uncertain_args[i]], -af.att_exists_var.at(af.uncertain_atts[j]) });
			}
		}
	}
	unordered_map<pair<uint32_t,uint32_t>,int> attacks_do_not_exist_var;
	for (uint32_t i = 0; i < af.symmetric_atts.size(); i++) {
		attacks_do_not_exist_var[af.symmetric_atts[i]] = ++count;
	}
	for (uint32_t i = 0; i < af.symmetric_atts.size(); i++) {
		if (!af.is_uncertain_arg[af.symmetric_atts[i].first] && !af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(af.symmetric_atts[i]) });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
			clauses.push_back({ attacks_do_not_exist_var[af.symmetric_atts[i]], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
		} else if (af.is_uncertain_arg[af.symmetric_atts[i].first] && !af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], af.arg_exists_var[af.symmetric_atts[i].first] });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(af.symmetric_atts[i]) });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
			clauses.push_back({ attacks_do_not_exist_var[af.symmetric_atts[i]], -af.arg_exists_var[af.symmetric_atts[i].first], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
		} else if (!af.is_uncertain_arg[af.symmetric_atts[i].first] && af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], af.arg_exists_var[af.symmetric_atts[i].second] });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(af.symmetric_atts[i]) });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
			clauses.push_back({ attacks_do_not_exist_var[af.symmetric_atts[i]], -af.arg_exists_var[af.symmetric_atts[i].second], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
		} else {
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], af.arg_exists_var[af.symmetric_atts[i].first] });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], af.arg_exists_var[af.symmetric_atts[i].second] });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(af.symmetric_atts[i]) });
			clauses.push_back({ -attacks_do_not_exist_var[af.symmetric_atts[i]], -af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
			clauses.push_back({ attacks_do_not_exist_var[af.symmetric_atts[i]], -af.arg_exists_var[af.symmetric_atts[i].first], -af.arg_exists_var[af.symmetric_atts[i].second], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) });
		}
	}
	vector<int> clause = { af.selector_var };
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			clause.push_back(source_exists_and_attack_does_not_var[af.uncertain_atts[i]]);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].second]) {
			clause.push_back(target_exists_and_attack_does_not_var[af.uncertain_atts[i]]);
		}
	}
	for (uint32_t i = 0; i < af.symmetric_atts.size(); i++) {
		clause.push_back(attacks_do_not_exist_var[af.symmetric_atts[i]]);
	}
	clauses.push_back(clause);
	return count;
}

void print_cred(const AF & af)
{
	vector<int> target = { af.target_var };
	vector<vector<int>> clauses = { target };
	if (af.sem == AD) {
		admissible(af, clauses);
	} else if (af.sem == CO) {
		complete(af, clauses);
	} else if (af.sem == ST) {
		stable(af, clauses);
	}
	vector<vector<int>> invalid_clauses;
	int count = invalid_completion(af, invalid_clauses);
	for (uint32_t i = 0; i < af.args; i++) {
		cout << "c x_" << af.int_to_arg[i] << " = " << af.arg_accepted_var[i] << "\n";
	}
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		cout << "c y_" << af.int_to_arg[af.uncertain_args[i]] << " = " << af.arg_exists_var[af.uncertain_args[i]] << "\n";
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		cout << "c r_" << af.int_to_arg[af.uncertain_atts[i].first] << "," << af.int_to_arg[af.uncertain_atts[i].second] <<  " = " << af.att_exists_var.at(af.uncertain_atts[i]) << "\n";
	}
	cout << "p cnf " << count << " " << clauses.size() + invalid_clauses.size() << "\n";
	if (af.control_args.size() != 0) {
		cout << "e ";
		for (uint32_t i = 0; i < af.control_args.size(); i++) {
			cout << af.arg_exists_var[af.control_args[i]] << " ";
		}
		cout << "0\n";
	}
	if (af.uncertain_args.size() - af.control_args.size() != 0 || af.uncertain_atts.size() != 0) {
		cout << "a ";
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			if (af.is_control_arg[af.uncertain_args[i]]) continue;
			cout << af.arg_exists_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.att_exists_var.at(af.uncertain_atts[i]) << " ";
		}
		cout << "0\n";
	}
	cout << "e ";
	for (uint32_t i = 0; i < af.args; i++) {
		cout << af.arg_accepted_var[i] << " ";
	}
	if (af.target_args.size() != 1) {
		cout << af.target_var << " ";
	}
	if (af.sem == AD || af.sem == CO) {
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			cout << af.arg_exists_and_accepted_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.source_accepted_var.at(af.uncertain_atts[i]) << " ";
		}
		for (uint32_t i = 0; i < af.args; i++) {
			cout << af.arg_rejected_var[i] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			cout << af.arg_exists_implies_rejected_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.source_rejected_var.at(af.uncertain_atts[i]) << " ";
		}
	} else if (af.sem == ST) {
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			cout << af.arg_exists_and_accepted_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.source_accepted_var.at(af.uncertain_atts[i]) << " ";
		}
#if defined(STB_AUX)
		for (uint32_t i = 0; i < af.args; i++) {
			cout << af.arg_rejected_var[i] << " ";
		}
#endif
	}
	cout << af.selector_var << " ";
	for (int i = af.count + 1; i <= count; i++) {
		cout << i << " ";
	}
	cout << "0\n";
	for (uint32_t i = 0; i < clauses.size(); i++) {
		print_clause_with_selector(clauses[i], af.selector_var);
	}
	for (uint32_t i = 0; i < invalid_clauses.size(); i++) {
		print_clause(invalid_clauses[i]);
	}
}

void print_skept(const AF & af)
{
	vector<int> target = { -af.target_var };
	vector<vector<int>> clauses = { target };
	if (af.sem == AD) {
		admissible(af, clauses);
	} else if (af.sem == CO) {
		complete(af, clauses);
	} else if (af.sem == ST) {
		stable(af, clauses);
	}
	for (uint32_t i = 0; i < af.args; i++) {
		cout << "c x_" << af.int_to_arg[i] << " = " << af.arg_accepted_var[i] << "\n";
	}
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		cout << "c y_" << af.int_to_arg[af.uncertain_args[i]] << " = " << af.arg_exists_var[af.uncertain_args[i]] << "\n";
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		cout << "c r_" << af.int_to_arg[af.uncertain_atts[i].first] << "," << af.int_to_arg[af.uncertain_atts[i].second] <<  " = " << af.att_exists_var.at(af.uncertain_atts[i]) << "\n";
	}
	cout << "p cnf " << af.count-1 << " " << clauses.size() << "\n";
	if (af.control_args.size() != 0) {
		cout << "a ";
		for (uint32_t i = 0; i < af.control_args.size(); i++) {
			cout << af.arg_exists_var[af.control_args[i]] << " ";
		}
		cout << "0\n";
	}
	cout << "e ";
	if (af.uncertain_args.size() - af.control_args.size() != 0 || af.uncertain_atts.size() != 0) {
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			if (af.is_control_arg[af.uncertain_args[i]]) continue;
			cout << af.arg_exists_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.att_exists_var.at(af.uncertain_atts[i]) << " ";
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		cout << af.arg_accepted_var[i] << " ";
	}
	if (af.target_args.size() != 1) {
		cout << af.target_var << " ";
	}
	if (af.sem == AD || af.sem == CO) {
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			cout << af.arg_exists_and_accepted_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.source_accepted_var.at(af.uncertain_atts[i]) << " ";
		}
		for (uint32_t i = 0; i < af.args; i++) {
			cout << af.arg_rejected_var[i] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			cout << af.arg_exists_implies_rejected_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.source_rejected_var.at(af.uncertain_atts[i]) << " ";
		}
	} else if (af.sem == ST) {
		for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
			cout << af.arg_exists_and_accepted_var[af.uncertain_args[i]] << " ";
		}
		for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
			cout << af.source_accepted_var.at(af.uncertain_atts[i]) << " ";
		}
#if defined(STB_AUX)
		for (uint32_t i = 0; i < af.args; i++) {
			cout << af.arg_rejected_var[i] << " ";
		}
#endif
	}
	cout << "0\n";
	for (uint32_t i = 0; i < clauses.size(); i++) {
		print_clause(clauses[i]);
	}
}

}
