/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Util.h"

#include <iostream>

using namespace std;

vector<int> completion_assumptions(const AF & af, const vector<uint8_t> & assignment, int additional_assumption)
{
	vector<int> assumptions(af.uncertain_args.size()+af.uncertain_atts.size()+1);
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		if (assignment[af.arg_exists_var[af.uncertain_args[i]]-1]) {
			assumptions[i] = af.arg_exists_var[af.uncertain_args[i]];
		} else {
			assumptions[i] = -af.arg_exists_var[af.uncertain_args[i]];
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (assignment[af.att_exists_var.at(af.uncertain_atts[i])-1]) {
			assumptions[af.uncertain_args.size()+i] = af.att_exists_var.at(af.uncertain_atts[i]);
		} else {
			assumptions[af.uncertain_args.size()+i] = -af.att_exists_var.at(af.uncertain_atts[i]);
		}
	}
	assumptions[af.uncertain_args.size()+af.uncertain_atts.size()] = additional_assumption;
	return assumptions;
}

vector<int> control_assumptions(const AF & af, const vector<uint8_t> & assignment, int additional_assumption)
{
	vector<int> assumptions(af.control_args.size()+1);
	for (uint32_t i = 0; i < af.control_args.size(); i++) {
		if (assignment[af.arg_exists_var[af.control_args[i]]-1]) {
			assumptions[i] = af.arg_exists_var[af.control_args[i]];
		} else {
			assumptions[i] = -af.arg_exists_var[af.control_args[i]];
		}
	}
	assumptions[af.control_args.size()] = additional_assumption;
	return assumptions;
}

vector<int> trivial_completion_refinement(const AF & af, const vector<uint8_t> & assignment)
{
	vector<int> refinement_clause(af.uncertain_args.size()+af.uncertain_atts.size());
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		if (assignment[af.arg_exists_var[af.uncertain_args[i]]-1]) {
			refinement_clause[i] = -af.arg_exists_var[af.uncertain_args[i]];
		} else {
			refinement_clause[i] = af.arg_exists_var[af.uncertain_args[i]];
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (assignment[af.att_exists_var.at(af.uncertain_atts[i])-1]) {
			refinement_clause[af.uncertain_args.size()+i] = -af.att_exists_var.at(af.uncertain_atts[i]);
		} else {
			refinement_clause[af.uncertain_args.size()+i] = af.att_exists_var.at(af.uncertain_atts[i]);
		}
	}
	return refinement_clause;
}

vector<int> optimized_completion_refinement(const AF & af, const vector<uint8_t> & assignment, semantics sem)
{
	vector<int> labeling(af.args);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i] || assignment[af.arg_exists_var[i]-1]) {
			if (assignment[af.arg_accepted_var[i]-1]) {
				labeling[i] = 1;
			} else if (sem == ST || assignment[af.arg_rejected_var[i]-1]) {
				labeling[i] = -1;
			} else {
				labeling[i] = 0;
			}
		}
	}
	vector<int> refinement_clause;
	refinement_clause.reserve(af.uncertain_args.size() + af.uncertain_atts.size());
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		if (af.is_control_arg[af.uncertain_args[i]]) {
			if (assignment[af.arg_exists_var[af.uncertain_args[i]]-1]) {
				refinement_clause.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
			} else {
				refinement_clause.push_back(af.arg_exists_var[af.uncertain_args[i]]);
			}
			continue;
		}
		if (assignment[af.arg_exists_var[af.uncertain_args[i]]-1]) {
			if (labeling[af.uncertain_args[i]] == 1) {
				refinement_clause.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
			}
			if ((sem == CO || sem == PR) && labeling[af.uncertain_args[i]] == 0) {
				refinement_clause.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
			}
		} else {
			bool add_argument = true;
			for (uint32_t j = 0; j < af.attackers[af.uncertain_args[i]].size(); j++) {
				if (labeling[af.attackers[af.uncertain_args[i]][j]] == 1) {
					add_argument = false;
					break;
				}
			}
			if (add_argument) {
				refinement_clause.push_back(af.arg_exists_var[af.uncertain_args[i]]);
			}
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (assignment[af.att_exists_var.at(af.uncertain_atts[i])-1]) {
			if (labeling[af.uncertain_atts[i].first] == 1 && labeling[af.uncertain_atts[i].second] == -1) {
				refinement_clause.push_back(-af.att_exists_var.at(af.uncertain_atts[i]));
			}
			if ((sem == CO || sem == PR) && labeling[af.uncertain_atts[i].first] == 0 && labeling[af.uncertain_atts[i].second] == 0) {
				refinement_clause.push_back(-af.att_exists_var.at(af.uncertain_atts[i]));
			}
		} else {
			if ((!af.is_uncertain_arg[af.uncertain_atts[i].first] || assignment[af.arg_exists_var[af.uncertain_atts[i].first]-1])
				&& (!af.is_uncertain_arg[af.uncertain_atts[i].second] || assignment[af.arg_exists_var[af.uncertain_atts[i].second]-1])) {
				if (labeling[af.uncertain_atts[i].first] == 1 && labeling[af.uncertain_atts[i].second] == 1) {
					refinement_clause.push_back(af.att_exists_var.at(af.uncertain_atts[i]));
				} else if (labeling[af.uncertain_atts[i].first] == 0 && labeling[af.uncertain_atts[i].second] == 1) {
					refinement_clause.push_back(af.att_exists_var.at(af.uncertain_atts[i]));
				} else if ((sem == CO || sem == PR) && labeling[af.uncertain_atts[i].first] == 1 && labeling[af.uncertain_atts[i].second] == 0) {
					refinement_clause.push_back(af.att_exists_var.at(af.uncertain_atts[i]));
				} else if (sem == PR && labeling[af.uncertain_atts[i].first] == 0 && labeling[af.uncertain_atts[i].second] == 0) {
					refinement_clause.push_back(af.att_exists_var.at(af.uncertain_atts[i]));
				}
			}
		}
	}
	return refinement_clause;
}

vector<int> trivial_control_refinement(const AF & af, const vector<uint8_t> & assignment)
{
	vector<int> refinement_clause(af.control_args.size());
	for (uint32_t i = 0; i < af.control_args.size(); i++) {
		if (assignment[af.arg_exists_var[af.control_args[i]]-1]) {
			refinement_clause[i] = -af.arg_exists_var[af.control_args[i]];
		} else {
			refinement_clause[i] = af.arg_exists_var[af.control_args[i]];
		}
	}
	return refinement_clause;
}

vector<int> optimized_control_refinement(const AF & af, const vector<uint8_t> & assignment, semantics sem)
{
	vector<int> labeling(af.args);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i] || assignment[af.arg_exists_var[i]-1]) {
			if (assignment[af.arg_accepted_var[i]-1]) {
				labeling[i] = 1;
			} else if (sem == ST || assignment[af.arg_rejected_var[i]-1]) {
				labeling[i] = -1;
			} else {
				labeling[i] = 0;
			}
		}
	}
	vector<int> refinement_clause;
	refinement_clause.reserve(af.control_args.size());
	for (uint32_t i = 0; i < af.control_args.size(); i++) {
		if (assignment[af.arg_exists_var[af.control_args[i]]-1]) {
			if (labeling[af.control_args[i]] == 1) {
				refinement_clause.push_back(-af.arg_exists_var[af.control_args[i]]);
			}
			if ((sem == CO || sem == PR) && labeling[af.control_args[i]] == 0) {
				refinement_clause.push_back(-af.arg_exists_var[af.control_args[i]]);
			}
		} else {
			bool add_argument = true;
			for (uint32_t j = 0; j < af.attackers[af.control_args[i]].size(); j++) {
				if (labeling[af.attackers[af.control_args[i]][j]] == 1) {
					add_argument = false;
					break;
				}
			}
			if (add_argument) {
				refinement_clause.push_back(af.arg_exists_var[af.control_args[i]]);
			}
		}
	}
	return refinement_clause;
}

vector<int> core_control_refinement(const AF & af, const vector<int> & conflict)
{
	vector<int> refinement_clause;
	for (uint32_t i = 0; i < conflict.size(); i++) {
		int var = abs(conflict[i]);
		if (var > (int)af.args && var <= (int)af.args + (int)af.uncertain_args.size()) {
			if (af.is_control_arg[af.arg_exists_var_to_arg.at(var)]) {
				refinement_clause.push_back(-conflict[i]);
			}
		}
	}
	return refinement_clause;
}


void print_completion(const AF & af, const vector<uint8_t> & assignment)
{
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			cout << "arg(" << af.int_to_arg.at(i) << ").\n";
		} else {
			if (assignment[af.arg_exists_var[i]-1]) {
				cout << "arg(" << af.int_to_arg.at(i) << ").\n";
			}
		}
	}
	for (uint32_t i = 0; i < af.atts.size(); i++) {
		uint32_t s = af.atts[i].first;
		uint32_t t = af.atts[i].second;
		if ((!af.is_uncertain_arg[s] || assignment[af.arg_exists_var[s]-1]) && (!af.is_uncertain_arg[t] || assignment[af.arg_exists_var[t]-1])) {
			cout << "att(" + af.int_to_arg.at(s) << "," << af.int_to_arg.at(t) << ").\n";
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		uint32_t s = af.uncertain_atts[i].first;
		uint32_t t = af.uncertain_atts[i].second;
		if (assignment[af.att_exists_var.at(af.uncertain_atts[i])-1]) {
			cout << "att(" + af.int_to_arg.at(s) << "," << af.int_to_arg.at(t) << ").\n";
		}
	}
}

void print_control_configuration(const AF & af, const vector<uint8_t> & assignment)
{
	for (uint32_t i = 0; i < af.control_args.size(); i++) {
		if (assignment[af.arg_exists_var[af.control_args[i]]-1]) {
			cout << af.int_to_arg.at(af.control_args[i]) << " ";
		}
	}
	cout << "\n";
}

void print_clause(const vector<int> & clause)
{
	for (uint32_t i = 0; i < clause.size(); i++) {
		cout << clause[i] << " ";
	}
	cout << "0\n";
}

void print_clause_with_selector(const vector<int> & clause, const int selector_var)
{
	cout << -selector_var << " ";
	for (uint32_t i = 0; i < clause.size(); i++) {
		cout << clause[i] << " ";
	}
	cout << "0\n";
}