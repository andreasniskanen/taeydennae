/*!
 * Copyright (c) <2019> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"
#include "Util.h"
#include "NecSkeptAcceptance.h"
#include <iostream>

using namespace std;

namespace NecSkeptAcceptance {

bool admissible(AF & af)
{
	return false;
}

bool admissible_ne(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.args);
	Encodings::add_admissible(af, solver);
	vector<int> ne_clause(af.args);
	for (uint32_t i = 0; i < af.args; i++) {
		ne_clause[i] = af.arg_accepted_var[i];
	}
	solver.add_clause(ne_clause);
	vector<int> query_arg_rejected = { -af.target_var };
	return !solver.solve(query_arg_rejected);
}

bool complete(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.args);
	Encodings::add_complete(af, solver);
	vector<int> query_arg_rejected = { -af.target_var };
	return !solver.solve(query_arg_rejected);
}

bool preferred(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_complete(af, solver);
	vector<int> query_arg_rejected = { -af.target_var };
	int count = 0;
	while (true) {
		count += 1;
		bool com_reject = solver.solve(query_arg_rejected);
		if (!com_reject) {
			cout << "c Iterations: " << count << "\n";
			return true;
		}
		
		vector<int> assumptions;
		assumptions.reserve(af.args + af.uncertain_args.size() + af.uncertain_atts.size());
		vector<int> complement_clause;
		complement_clause.reserve(af.args + af.uncertain_args.size() + af.uncertain_atts.size());
		
		while (true) {
			assumptions.clear();
			complement_clause.clear();
			assumptions.push_back(-af.target_var);
			for (uint32_t i = 0; i < af.args; i++) {
				if (solver.assignment[af.arg_accepted_var[i]-1]) {
					assumptions.push_back(af.arg_accepted_var[i]);
				} else {
					complement_clause.push_back(af.arg_accepted_var[i]);
				}
			}
			for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
				if (solver.assignment[af.arg_exists_var[af.uncertain_args[i]]-1]) {
					assumptions.push_back(af.arg_exists_var[af.uncertain_args[i]]);
					complement_clause.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
				} else {
					assumptions.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
					complement_clause.push_back(af.arg_exists_var[af.uncertain_args[i]]);
				}
			}
			for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
				if (solver.assignment[af.att_exists_var[af.uncertain_atts[i]]-1]) {
					assumptions.push_back(af.att_exists_var[af.uncertain_atts[i]]);
					complement_clause.push_back(-af.att_exists_var[af.uncertain_atts[i]]);
				} else {
					assumptions.push_back(-af.att_exists_var[af.uncertain_atts[i]]);
					complement_clause.push_back(af.att_exists_var[af.uncertain_atts[i]]);
				}
			}
			solver.add_clause(complement_clause);
			bool superset_exists = solver.solve(assumptions);
			if (!superset_exists) break;
		}
		assumptions[0] = af.target_var;
		if (!solver.solve(assumptions)) {
			cout << "c Iterations: " << count << "\n";
			return false;
		}
#if defined(REFINEMENT)
		vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, AD);
		for (uint32_t i = 0; i < af.args; i++) {
			if (!solver.assignment[af.arg_accepted_var[i]-1]) {
				blocking_clause.push_back(af.arg_accepted_var[i]);
			}
		}
		solver.add_clause(blocking_clause);
#endif
	}
}

bool stable(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.args);
	Encodings::add_stable(af, solver);
	vector<int> query_arg_rejected = { -af.target_var };
	return !solver.solve(query_arg_rejected);
}

bool grounded(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.args);
	Encodings::add_grounded(af, solver);
	vector<int> query_arg_rejected = { -af.target_var };
	return !solver.solve(query_arg_rejected);
}

}