/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "AF.h"
#include "PosCredAcceptance.h"
#include "NecCredAcceptance.h"
#include "PosSkeptAcceptance.h"
#include "NecSkeptAcceptance.h"
#include "PosExSkeptAcceptance.h"
#include "NecExSkeptAcceptance.h"
#include "CredControl.h"
#include "SkeptControl.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>
#include <getopt.h>

using namespace std;

static int formats_flag = 0;
static int problems_flag = 0;
static int version_flag = 0;

task string_to_task(string problem)
{
	string tmp = problem.substr(0, problem.find("-"));
	if (tmp == "PCA") return PCA;
	if (tmp == "NCA") return NCA;
	if (tmp == "PSA") return PSA;
	if (tmp == "NSA") return NSA;
	if (tmp == "PEXSA") return PEXSA;
	if (tmp == "NEXSA") return NEXSA;
	if (tmp == "CC") return CC;
	if (tmp == "SC") return SC;
	return UNKNOWN_TASK;
}

semantics string_to_sem(string problem)
{
	problem.erase(0, problem.find("-") + 1);
	string tmp = problem.substr(0, problem.find("-"));
	if (tmp == "AD") return AD;
	if (tmp == "NEAD") return NEAD;
	if (tmp == "CO") return CO;
	if (tmp == "PR") return PR;
	if (tmp == "ST") return ST;
	if (tmp == "GR") return GR;
	return UNKNOWN_SEM;
}

void print_usage(string solver_name)
{
	cout << "Usage: " << solver_name << " [options] -p <task> -f <file> -a <query>\n";
	cout << "\n";
	cout << "  <task>      computational task in the form (PCA|NCA|PSA|NSA|PEXSA|NEXSA|CC|SC)-(AD|NEAD|CO|PR|ST|GR)\n";
	cout << "  <file>      input incomplete or control AF in apx format extended with ?arg, ?att, satt, and carg predicates\n";
	cout << "  <query>     query argument\n";
	cout << "\n";
	cout << "Options:\n";
	cout << "  --formats   Prints available file formats.\n";
	cout << "  --problems  Prints available computational tasks.\n";
	cout << "  --version   Prints version and author information.\n";
	cout << "  --qdimacs   QDIMACS output (for controllability problems).\n";
}

void print_formats()
{
	cout << "[apx]\n";
}

void print_problems()
{
	vector<string> tasks = {"PCA","NCA","PSA","NSA","PEXSA","NEXSA","CC","SC"};
	vector<string> sems = {"AD","NEAD","CO","PR","ST","GR"};
	cout << "[";
	for (uint32_t i = 0; i < tasks.size(); i++) {
		for (uint32_t j = 0; j < sems.size(); j++) {
			string problem = tasks[i] + "-" + sems[j];
			if (problem == "CC-NEAD" || problem == "SC-NEAD") continue;
			if (problem != "SC-GR") cout << problem << ",";
			else cout << problem;
		}
	}
	cout << "]\n";
}

void print_version(string solver_name)
{
	cout << solver_name << " version 2020.08.10\n" << "Andreas Niskanen, University of Helsinki <andreas.niskanen@helsinki.fi>\n";
}

int main(int argc, char ** argv)
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);

	if (argc == 1) {
		print_usage(argv[0]);
		return 0;
	}

	const struct option longopts[] =
	{
		{"formats", no_argument, &formats_flag, 1},
		{"problems", no_argument, &problems_flag, 1},
		{"version", no_argument, &version_flag, 1},
		{"p", required_argument, 0, 'p'},
		{"f", required_argument, 0, 'f'},
		{"a", required_argument, 0, 'a'},
		{"qdimacs", no_argument, 0, 'q'},
		{0, 0, 0, 0}
	};

	int option_index = 0;
	int opt = 0;
	string task, file, fileformat, query;
	bool qdimacs_output = false;

	while ((opt = getopt_long_only(argc, argv, "", longopts, &option_index)) != -1) {
		switch (opt) {
			case 0:
				break;
			case 'p':
				task = optarg;
				break;
			case 'f':
				file = optarg;
				break;
			case 'a':
				query = optarg;
				break;
			case 'q':
				qdimacs_output = true;
				break;
			default:
				print_usage(argv[0]);
				return EXIT_FAILURE;
		}
	}

	if (formats_flag) {
		print_formats();
		return EXIT_SUCCESS;
	}

	if (problems_flag) {
		print_problems();
		return EXIT_SUCCESS;
	}

	if (version_flag) {
		print_version(argv[0]);
		return EXIT_SUCCESS;
	}

	if (task.empty()) {
		cerr << argv[0] << ": Task must be specified via -p flag\n";
		return EXIT_FAILURE;
	}

	if (file.empty()) {
		cerr << argv[0] << ": Input file must be specified via -f flag\n";
		return EXIT_FAILURE;
	}

	if (query.empty()) {
		cerr << argv[0] << ": Query argument must be specified via -a flag\n";
		return EXIT_FAILURE;
	}

	ifstream input;
	input.open(file);

	if (!input.good()) {
		cerr << argv[0] << ": Cannot open input file\n";
		return EXIT_FAILURE;
	}

	AF af = AF();
	af.sem = string_to_sem(task);
	af.qdimacs_output = qdimacs_output;
	vector<pair<string,string>> atts;
	vector<pair<string,string>> uncertain_atts;
	vector<pair<string,string>> symmetric_atts;
	string line, arg, source, target;
	
	while (!input.eof()) {
		getline(input, line);
		line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());
		if (line.length() == 0 || line[0] == '/' || line[0] == '%') continue;
		if (line.length() < 6) cerr << "Warning: Cannot parse line: " << line << "\n";
		string op = line.substr(0,3);
		string longop = line.substr(0,4);
		if (op == "arg") {
			if (line[3] == '(' && line.find(')') != string::npos) {
				arg = line.substr(4,line.find(')')-4);
				af.add_argument(arg);
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else if (op == "att") {
			if (line[3] == '(' && line.find(',') != string::npos && line.find(')') != string::npos) {
				source = line.substr(4,line.find(',')-4);
				target = line.substr(line.find(',')+1,line.find(')')-line.find(',')-1);
				atts.push_back(make_pair(source,target));
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else if (longop == "?arg") {
			if (line[4] == '(' && line.find(')') != string::npos) {
				arg = line.substr(5,line.find(')')-5);
				af.add_uncertain_argument(arg);
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else if (longop == "?att") {
			if (line[4] == '(' && line.find(',') != string::npos && line.find(')') != string::npos) {
				source = line.substr(5,line.find(',')-5);
				target = line.substr(line.find(',')+1,line.find(')')-line.find(',')-1);
				uncertain_atts.push_back(make_pair(source,target));
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else if (longop == "satt") {
			if (line[4] == '(' && line.find(',') != string::npos && line.find(')') != string::npos) {
				source = line.substr(5,line.find(',')-5);
				target = line.substr(line.find(',')+1,line.find(')')-line.find(',')-1);
				symmetric_atts.push_back(make_pair(source,target));
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else if (longop == "carg") {
			if (line[4] == '(' && line.find(')') != string::npos) {
				arg = line.substr(5,line.find(')')-5);
				af.add_control_argument(arg);
			} else {
				cerr << "Warning: Cannot parse line: " << line << "\n";
			}
		} else {
			cerr << "Warning: Cannot parse line: " << line << "\n";
		}
	}

	input.close();

	af.initialize_att_containers();

	for (uint32_t i = 0; i < atts.size(); i++) {
		af.add_attack(atts[i]);
	}

	for (uint32_t i = 0; i < uncertain_atts.size(); i++) {
		af.add_uncertain_attack(uncertain_atts[i]);
	}

	for (uint32_t i = 0; i < symmetric_atts.size(); i++) {
		af.add_symmetric_attack(symmetric_atts[i]);
	}

	af.set_target(query, ",");

	if (af.target_args.size() == 0) {
		cerr << argv[0] << ": No valid target arguments specified\n";
		return EXIT_FAILURE;
	}

	af.initialize_vars(string_to_sem(task) == GR && string_to_task(task) != CC && string_to_task(task) != SC);

	switch (string_to_task(task)) {
		case PCA:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = PosCredAcceptance::admissible(af);
					break;
				case NEAD:
					accepted = PosCredAcceptance::admissible(af);
					break;
				case CO:
					accepted = PosCredAcceptance::complete(af);
					break;
				case PR:
					accepted = PosCredAcceptance::preferred(af);
					break;
				case ST:
					accepted = PosCredAcceptance::stable(af);
					break;
				case GR:
					accepted = PosCredAcceptance::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		case NCA:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = NecCredAcceptance::admissible(af);
					break;
				case NEAD:
					accepted = NecCredAcceptance::admissible(af);
					break;
				case CO:
					accepted = NecCredAcceptance::complete(af);
					break;
				case PR:
					accepted = NecCredAcceptance::preferred(af);
					break;
				case ST:
					accepted = NecCredAcceptance::stable(af);
					break;
				case GR:
					accepted = NecCredAcceptance::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		case PSA:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = PosExSkeptAcceptance::admissible(af);
					break;
				case NEAD:
					accepted = PosSkeptAcceptance::admissible_ne(af);
					break;
				case CO:
					accepted = PosExSkeptAcceptance::complete(af);
					break;
				case PR:
					accepted = PosExSkeptAcceptance::preferred(af);
					break;
				case ST:
					accepted = PosSkeptAcceptance::stable(af);
					break;
				case GR:
					accepted = PosExSkeptAcceptance::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		case NSA:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = NecSkeptAcceptance::admissible_ne(af);
					break;
				case NEAD:
					accepted = NecSkeptAcceptance::admissible_ne(af);
					break;
				case CO:
					accepted = NecSkeptAcceptance::complete(af);
					break;
				case PR:
					accepted = NecSkeptAcceptance::preferred(af);
					break;
				case ST:
					accepted = NecSkeptAcceptance::stable(af);
					break;
				case GR:
					accepted = NecSkeptAcceptance::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		case PEXSA:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = PosExSkeptAcceptance::admissible(af);
					break;
				case NEAD:
					accepted = PosExSkeptAcceptance::admissible_ne(af);
					break;
				case CO:
					accepted = PosExSkeptAcceptance::complete(af);
					break;
				case PR:
					accepted = PosExSkeptAcceptance::preferred(af);
					break;
				case ST:
					accepted = PosExSkeptAcceptance::stable(af);
					break;
				case GR:
					accepted = PosExSkeptAcceptance::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		case NEXSA:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = NecSkeptAcceptance::admissible(af);
					break;
				case NEAD:
					accepted = NecExSkeptAcceptance::admissible_ne(af);
					break;
				case CO:
					accepted = NecSkeptAcceptance::complete(af);
					break;
				case PR:
					accepted = NecSkeptAcceptance::preferred(af);
					break;
				case ST:
					accepted = NecExSkeptAcceptance::stable(af);
					break;
				case GR:
					accepted = NecSkeptAcceptance::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		case CC:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = CredControl::admissible(af);
					break;
				case CO:
					accepted = CredControl::complete(af);
					break;
				case PR:
					accepted = CredControl::preferred(af);
					break;
				case ST:
					accepted = CredControl::stable(af);
					break;
				case GR:
					accepted = CredControl::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			if (!af.qdimacs_output) cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		case SC:
		{
			bool accepted = false;
			switch (string_to_sem(task)) {
				case AD:
					accepted = SkeptControl::admissible(af);
					break;
				case CO:
					accepted = SkeptControl::complete(af);
					break;
				case PR:
					accepted = SkeptControl::preferred(af);
					break;
				case ST:
					accepted = SkeptControl::stable(af);
					break;
				case GR:
					accepted = SkeptControl::grounded(af);
					break;
				default:
					cerr << argv[0] << ": Unsupported semantics\n";
					return EXIT_FAILURE;
			}
			if (!af.qdimacs_output) cout << (accepted ? "YES" : "NO") << "\n";
			break;
		}
		default:
			cerr << argv[0] << ": Unsupported problem\n";
			return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
