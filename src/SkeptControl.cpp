/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"
#include "QDIMACS.h"
#include "Util.h"
#include "SkeptControl.h"

#include <iostream>

using namespace std;

namespace SkeptControl {

bool admissible(AF & af)
{
	return false;
}

bool complete(AF & af)
{
	if (af.qdimacs_output) {
		QDIMACS::print_skept(af);
		return false;
	}
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_complete(af, solver);
	uint32_t iters = 0;
	vector<int> query_arg_accepted = { af.target_var };
	while (true) {
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		iters++;
		if (!pos_cred_accept) {
			cout << "c Iterations: " << iters << "\n";
			return false;
		}
		vector<int> assumptions = control_assumptions(af, solver.assignment, -af.target_var);
		bool nec_skept_accept = solver.solve(assumptions);
		if (!nec_skept_accept) {
			cout << "c Iterations: " << iters << "\n";
			print_control_configuration(af, solver.assignment);
			return true;
		}
#if defined(REFINEMENT)
		vector<int> refinement = optimized_control_refinement(af, solver.assignment, CO);
#else
		vector<int> refinement = trivial_control_refinement(af, solver.assignment);
#endif
		solver.add_clause(refinement);
	}
}

bool preferred(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_complete(af, solver);
	uint32_t iters = 0;
	vector<int> query_arg_accepted = { af.target_var};
	while (true) {
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		iters++;
		if (!pos_cred_accept) {
			cout << "c Iterations: " << iters << "\n";
			return false;
		}
		vector<int> control = control_assumptions(af, solver.assignment, -af.target_var);
		bool nec_skept_accept = solver.solve(control);
		if (!nec_skept_accept) {
			cout << "c Iterations: " << iters << "\n";
			print_control_configuration(af, solver.assignment);
			return true;
		}
		vector<int> assumptions;
		assumptions.reserve(af.args + af.uncertain_args.size() + af.uncertain_atts.size());
		vector<int> complement_clause;
		complement_clause.reserve(af.args + af.uncertain_args.size() + af.uncertain_atts.size());
		while (true) {
			assumptions.clear();
			complement_clause.clear();
			assumptions.push_back(-af.target_var);
			for (uint32_t i = 0; i < af.args; i++) {
				if (solver.assignment[af.arg_accepted_var[i]-1]) {
					assumptions.push_back(af.arg_accepted_var[i]);
				} else {
					complement_clause.push_back(af.arg_accepted_var[i]);
				}
			}
			for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
				if (solver.assignment[af.arg_exists_var[af.uncertain_args[i]]-1]) {
					assumptions.push_back(af.arg_exists_var[af.uncertain_args[i]]);
					complement_clause.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
				} else {
					assumptions.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
					complement_clause.push_back(af.arg_exists_var[af.uncertain_args[i]]);
				}
			}
			for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
				if (solver.assignment[af.att_exists_var[af.uncertain_atts[i]]-1]) {
					assumptions.push_back(af.att_exists_var[af.uncertain_atts[i]]);
					complement_clause.push_back(-af.att_exists_var[af.uncertain_atts[i]]);
				} else {
					assumptions.push_back(-af.att_exists_var[af.uncertain_atts[i]]);
					complement_clause.push_back(af.att_exists_var[af.uncertain_atts[i]]);
				}
			}
			solver.add_clause(complement_clause);
			bool superset_exists = solver.solve(assumptions);
			if (!superset_exists) break;
		}
		assumptions[0] = af.target_var;
		if (!solver.solve(assumptions)) {
#if defined(REFINEMENT)
			vector<int> refinement = optimized_control_refinement(af, solver.assignment, PR);
#else
			vector<int> refinement = trivial_control_refinement(af, solver.assignment);
#endif
			solver.add_clause(refinement);
		}
	}
}

bool stable(AF & af)
{
	if (af.qdimacs_output) {
		QDIMACS::print_skept(af);
		return false;
	}
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_stable_with_selector(af, solver);
	uint32_t iters = 0;
	vector<int> query_arg_accepted = { af.selector_var, af.target_var };
	while (true) {
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		iters++;
		if (!pos_cred_accept) {
			vector<int> query_arg_rejected = { af.selector_var, -af.target_var };
			while (solver.solve(query_arg_rejected)) {
				iters++;
#if defined(REFINEMENT)
				vector<int> refinement = optimized_control_refinement(af, solver.assignment, ST);
#else
				vector<int> refinement = trivial_control_refinement(af, solver.assignment);
#endif
				solver.add_clause(refinement);
			}
			vector<int> select = { -af.selector_var };
			bool control_with_no_ext = solver.solve(select);
			if (control_with_no_ext) {
				cout << "c Iterations: " << iters << "\n";
				print_control_configuration(af, solver.assignment);
				return true;
			} else {
				cout << "c Iterations: " << iters << "\n";
				return false;
			}
		}
		vector<int> assumptions = control_assumptions(af, solver.assignment, -af.target_var);
		assumptions.push_back(af.selector_var);
		bool nec_skept_accept = solver.solve(assumptions);
		if (!nec_skept_accept) {
			cout << "c Iterations: " << iters << "\n";
			print_control_configuration(af, solver.assignment);
			return true;
		}
#if defined(REFINEMENT)
		vector<int> refinement = optimized_control_refinement(af, solver.assignment, ST);
#else
		vector<int> refinement = trivial_control_refinement(af, solver.assignment);
#endif
		solver.add_clause(refinement);
	}
}

bool grounded(AF & af)
{
	return complete(af);
}

}