/*!
 * Copyright (c) <2019> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"
#include "Util.h"
#include "PosExSkeptAcceptance.h"
#include <iostream>

using namespace std;

namespace PosExSkeptAcceptance {

bool admissible(AF & af)
{
	return false;
}

bool admissible_ne(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_admissible(af, solver);
	vector<int> ne_clause(af.args);
	for (uint32_t i = 0; i < af.args; i++) {
		ne_clause[i] = af.arg_accepted_var[i];
	}
	solver.add_clause(ne_clause);
	vector<int> query_arg_accepted = { af.target_var };
	int count = 0;
	while (true) {
		count += 1;
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		if (!pos_cred_accept) {
			cout << "c Iterations: " << count << "\n";
			return false;
		}
		vector<int> assumptions = completion_assumptions(af, solver.assignment, -af.target_var);
		bool pos_skept_reject = solver.solve(assumptions);
		if (!pos_skept_reject) {
			cout << "c Iterations: " << count << "\n";
			return true;
		}
#if defined(REFINEMENT)
		vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, NEAD);
#else
		vector<int> blocking_clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(blocking_clause);
	}
}

// TODO: use grounded?
bool complete(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_complete(af, solver);
	vector<int> query_arg_accepted = { af.target_var };
	int count = 0;
	while (true) {
		count += 1;
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		if (!pos_cred_accept) {
			cout << "c Iterations: " << count << "\n";
			return false;
		}
		vector<int> assumptions = completion_assumptions(af, solver.assignment, -af.target_var);
		bool pos_skept_reject = solver.solve(assumptions);
		if (!pos_skept_reject) {
			cout << "c Iterations: " << count << "\n";
			return true;
		}
#if defined(REFINEMENT)
		vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, CO);
#else
		vector<int> blocking_clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(blocking_clause);
	}
}

bool preferred(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_complete(af, solver);
	vector<int> query_arg_accepted = { af.target_var };
	int count = 0;
	while (true) {
		count += 1;
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		if (!pos_cred_accept) {
			cout << "c Iterations: " << count << "\n";
			return false;
		}
		vector<int> assumptions = completion_assumptions(af, solver.assignment, -af.target_var);
		bool pos_skept_reject = solver.solve(assumptions);
		if (!pos_skept_reject) {
			cout << "c Iterations: " << count << "\n";
			return true;
		}
		vector<int> complement_clause;
		complement_clause.reserve(af.args + af.uncertain_args.size() + af.uncertain_atts.size());
		while (true) {
			assumptions.clear();
			complement_clause.clear();
			assumptions.push_back(-af.target_var);
			for (uint32_t i = 0; i < af.args; i++) {
				if (solver.assignment[af.arg_accepted_var[i]-1]) {
					assumptions.push_back(af.arg_accepted_var[i]);
				} else {
					complement_clause.push_back(af.arg_accepted_var[i]);
				}
			}
			for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
				if (solver.assignment[af.arg_exists_var[af.uncertain_args[i]]-1]) {
					assumptions.push_back(af.arg_exists_var[af.uncertain_args[i]]);
					complement_clause.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
				} else {
					assumptions.push_back(-af.arg_exists_var[af.uncertain_args[i]]);
					complement_clause.push_back(af.arg_exists_var[af.uncertain_args[i]]);
				}
			}
			for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
				if (solver.assignment[af.att_exists_var[af.uncertain_atts[i]]-1]) {
					assumptions.push_back(af.att_exists_var[af.uncertain_atts[i]]);
					complement_clause.push_back(-af.att_exists_var[af.uncertain_atts[i]]);
				} else {
					assumptions.push_back(-af.att_exists_var[af.uncertain_atts[i]]);
					complement_clause.push_back(af.att_exists_var[af.uncertain_atts[i]]);
				}
			}
			solver.add_clause(complement_clause);
			bool superset_exists = solver.solve(assumptions);
			if (!superset_exists) break;
		}
		assumptions[0] = af.target_var;
		if (!solver.solve(assumptions)) {
#if defined(REFINEMENT)
			vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, PR);
#else
			vector<int> blocking_clause = trivial_completion_refinement(af, solver.assignment);
#endif
			solver.add_clause(blocking_clause);
		}
	}
}

bool stable(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_stable(af, solver);
	vector<int> query_arg_accepted = { af.target_var };
	int count = 0;
	while (true) {
		count += 1;
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		if (!pos_cred_accept) {
			cout << "c Iterations: " << count << "\n";
			return false;
		}
		vector<int> assumptions = completion_assumptions(af, solver.assignment, -af.target_var);
		bool pos_skept_reject = solver.solve(assumptions);
		if (!pos_skept_reject) {
			cout << "c Iterations: " << count << "\n";
			return true;
		}
#if defined(REFINEMENT)
		vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, ST);
#else
		vector<int> blocking_clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(blocking_clause);
	}
}

bool grounded(AF & af)
{
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_grounded(af, solver);
	vector<int> query_arg_accepted = { af.target_var };
	return solver.solve(query_arg_accepted);
}

}