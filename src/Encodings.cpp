/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"

using namespace std;

namespace Encodings {

void add_arg_exists_and_accepted_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.arg_exists_and_accepted_var[i], af.arg_accepted_var[i] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.arg_exists_and_accepted_var[i], af.arg_exists_var[i] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_exists_and_accepted_var[i], -af.arg_accepted_var[i], -af.arg_exists_var[i] };
			solver.add_clause(clause);
		}
	}
}

void add_source_accepted_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { -af.source_accepted_var.at(af.uncertain_atts[i]), af.att_exists_var.at(af.uncertain_atts[i]) };
		solver.add_clause(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { -af.source_accepted_var.at(af.uncertain_atts[i]), af.arg_accepted_var[af.uncertain_atts[i].first] };
		solver.add_clause(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { -af.source_accepted_var.at(af.uncertain_atts[i]), af.arg_exists_var[af.uncertain_atts[i].first] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (!af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { af.source_accepted_var.at(af.uncertain_atts[i]), -af.arg_accepted_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			solver.add_clause(clause);
		} else {
			vector<int> clause = { af.source_accepted_var.at(af.uncertain_atts[i]), -af.arg_accepted_var[af.uncertain_atts[i].first], -af.arg_exists_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			solver.add_clause(clause);
		}
	}
}

void add_arg_rejected_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.attackers[i][j]]) {
				vector<int> clause = { af.arg_rejected_var[i], -af.arg_accepted_var[af.attackers[i][j]] };
				solver.add_clause(clause);
			} else {
				vector<int> clause = { af.arg_rejected_var[i], -af.arg_exists_and_accepted_var[af.attackers[i][j]] };
				solver.add_clause(clause);
			}
		}
		for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
			vector<int> clause = { af.arg_rejected_var[i], -af.source_accepted_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
			solver.add_clause(clause);
		}
		vector<int> clause(af.attackers[i].size() + af.uncertain_attackers[i].size() + 1);
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.attackers[i][j]]) {
				clause[j] = af.arg_accepted_var[af.attackers[i][j]];
			} else {
				clause[j] = af.arg_exists_and_accepted_var[af.attackers[i][j]];
			}
		}
		for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
			clause[j+af.attackers[i].size()] = af.source_accepted_var.at(make_pair(af.uncertain_attackers[i][j], i));
		}
		clause[af.attackers[i].size() + af.uncertain_attackers[i].size()] = -af.arg_rejected_var[i];
		solver.add_clause(clause);
	}
}

void add_arg_exists_implies_rejected_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_exists_implies_rejected_var[i], -af.arg_rejected_var[i] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_exists_implies_rejected_var[i], af.arg_exists_var[i] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.arg_exists_implies_rejected_var[i], af.arg_rejected_var[i], -af.arg_exists_var[i] };
			solver.add_clause(clause);
		}
	}
}

void add_source_rejected_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { af.source_rejected_var.at(af.uncertain_atts[i]), -af.arg_rejected_var[af.uncertain_atts[i].first] };
		solver.add_clause(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		vector<int> clause = { af.source_rejected_var.at(af.uncertain_atts[i]), af.att_exists_var.at(af.uncertain_atts[i]) };
		solver.add_clause(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { af.source_rejected_var.at(af.uncertain_atts[i]), af.arg_exists_var[af.uncertain_atts[i].first] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (!af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { -af.source_rejected_var.at(af.uncertain_atts[i]), af.arg_rejected_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			solver.add_clause(clause);
		} else {
			vector<int> clause = { -af.source_rejected_var.at(af.uncertain_atts[i]), af.arg_rejected_var[af.uncertain_atts[i].first], -af.arg_exists_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			solver.add_clause(clause);
		}
	}
}

void add_target_clauses(const AF & af, SAT_Solver & solver) {
	if (af.target_args.size() == 1) {
		return;
	}
	for (uint32_t i = 0; i < af.target_args.size(); i++) {
		vector<int> clause = { -af.target_var, af.arg_accepted_var[af.target_args[i]] };
		solver.add_clause(clause);
	}
	vector<int> clause = { af.target_var };
	for (uint32_t i = 0; i < af.target_args.size(); i++) {
		clause.push_back(-af.arg_accepted_var[af.target_args[i]]);
	}
	solver.add_clause(clause);
}

void add_additional_clauses(const AF & af, SAT_Solver & solver) {
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		vector<int> clause = { af.arg_exists_var[af.uncertain_args[i]], -af.arg_accepted_var[af.uncertain_args[i]] };
		solver.add_clause(clause);
	}
	for (uint32_t i = 0; i < af.uncertain_args.size(); i++) {
		for (uint32_t j = 0; j < af.uncertain_atts.size(); j++) {
			if (af.uncertain_atts[j].first == af.uncertain_args[i] || af.uncertain_atts[j].second == af.uncertain_args[i]) {
				vector<int> clause = { af.arg_exists_var[af.uncertain_args[i]], -af.att_exists_var.at(af.uncertain_atts[j]) };
				solver.add_clause(clause);
			}
		}
	}   
}

void add_symmetric_clauses(const AF & af, SAT_Solver & solver) {
	for (uint32_t i = 0; i < af.symmetric_atts.size(); i++) {
		if (!af.is_uncertain_arg[af.symmetric_atts[i].first] && !af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			vector<int> clause = { af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			solver.add_clause(clause);
		} else if (af.is_uncertain_arg[af.symmetric_atts[i].first] && !af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			vector<int> clause = { -af.arg_exists_var[af.symmetric_atts[i].first], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			solver.add_clause(clause);
		} else if (!af.is_uncertain_arg[af.symmetric_atts[i].first] && af.is_uncertain_arg[af.symmetric_atts[i].second]) {
			vector<int> clause = { -af.arg_exists_var[af.symmetric_atts[i].second], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			solver.add_clause(clause);
		} else {
			vector<int> clause = { -af.arg_exists_var[af.symmetric_atts[i].first], -af.arg_exists_var[af.symmetric_atts[i].second], af.att_exists_var.at(af.symmetric_atts[i]), af.att_exists_var.at(make_pair(af.symmetric_atts[i].second, af.symmetric_atts[i].first)) };
			solver.add_clause(clause);
		}
	}
}

void add_conflict_free(const AF & af, SAT_Solver & solver)
{
	add_target_clauses(af, solver);
	add_additional_clauses(af, solver);
	add_symmetric_clauses(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			for (uint32_t j = 0;  j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_accepted_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_exists_var[af.attackers[i][j]], -af.arg_accepted_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				}
			}
			for (uint32_t j = 0;  j < af.uncertain_attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.uncertain_attackers[i][j]]) {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_exists_var[af.uncertain_attackers[i][j]], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					solver.add_clause(clause);
				}
			}
		} else {
			for (uint32_t j = 0;  j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_accepted_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_exists_var[af.attackers[i][j]], -af.arg_accepted_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				}
			}
			for (uint32_t j = 0;  j < af.uncertain_attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.uncertain_attackers[i][j]]) {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)), -af.arg_accepted_var[i], -af.arg_exists_var[i], -af.arg_exists_var[af.uncertain_attackers[i][j]], -af.arg_accepted_var[af.uncertain_attackers[i][j]] };
					solver.add_clause(clause);
				}
			}
		}
	}
}

void add_admissible(const AF & af, SAT_Solver & solver)
{
	add_conflict_free(af, solver);
	add_arg_exists_and_accepted_clauses(af, solver);
	add_source_accepted_clauses(af, solver);
	add_arg_rejected_clauses(af, solver);
	add_arg_exists_implies_rejected_clauses(af, solver);
	add_source_rejected_clauses(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_accepted_var[i], af.arg_rejected_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.arg_accepted_var[i], af.arg_exists_implies_rejected_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				vector<int> clause = { -af.arg_accepted_var[i], af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
				solver.add_clause(clause);
			}
		} else {
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.arg_exists_var[i], -af.arg_accepted_var[i], af.arg_rejected_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.arg_exists_var[i], -af.arg_accepted_var[i], af.arg_exists_implies_rejected_var[af.attackers[i][j]] };
					solver.add_clause(clause);
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				vector<int> clause = { -af.arg_exists_var[i], -af.arg_accepted_var[i], af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
				solver.add_clause(clause);
			}
		}
	}
}

void add_complete(const AF & af, SAT_Solver & solver)
{
	add_admissible(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			vector<int> clause(af.attackers[i].size()+af.uncertain_attackers[i].size()+1);
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					clause[j] = -af.arg_rejected_var[af.attackers[i][j]];
				} else {
					clause[j] = -af.arg_exists_implies_rejected_var[af.attackers[i][j]];
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				clause[j+af.attackers[i].size()] = -af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i));
			}
			clause[af.attackers[i].size() + af.uncertain_attackers[i].size()] = af.arg_accepted_var[i];
			solver.add_clause(clause);
		} else {
			vector<int> clause(af.attackers[i].size()+af.uncertain_attackers[i].size()+2);
			clause[0] = -af.arg_exists_var[i];
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					clause[j+1] = -af.arg_rejected_var[af.attackers[i][j]];
				} else {
					clause[j+1] = -af.arg_exists_implies_rejected_var[af.attackers[i][j]];
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				clause[j+af.attackers[i].size()+1] = -af.source_rejected_var.at(make_pair(af.uncertain_attackers[i][j], i));
			}
			clause[af.attackers[i].size() + af.uncertain_attackers[i].size()+1] = af.arg_accepted_var[i];
			solver.add_clause(clause);
		}
	}
}

void add_stable(const AF & af, SAT_Solver & solver)
{
	add_conflict_free(af, solver);
	add_arg_exists_and_accepted_clauses(af, solver);
	add_source_accepted_clauses(af, solver);
	add_arg_rejected_clauses(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			vector<int> clause = { af.arg_accepted_var[i], af.arg_rejected_var[i] };
			solver.add_clause(clause);
		} else {
			vector<int> clause = { -af.arg_exists_var[i], af.arg_accepted_var[i], af.arg_rejected_var[i] };
			solver.add_clause(clause);
		}
	}
}

void add_stable_with_selector(const AF & af, SAT_Solver & solver)
{
	add_conflict_free(af, solver);
	add_arg_exists_and_accepted_clauses(af, solver);
	add_source_accepted_clauses(af, solver);
	add_arg_rejected_clauses(af, solver);
	for (uint32_t i = 0; i < af.args; i++) {
		if (!af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.selector_var, af.arg_accepted_var[i], af.arg_rejected_var[i] };
			solver.add_clause(clause);
		} else {
			vector<int> clause = { -af.selector_var, -af.arg_exists_var[i], af.arg_accepted_var[i], af.arg_rejected_var[i] };
			solver.add_clause(clause);
		}
	}
}

void add_source_and_att_exist_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { -af.source_and_att_exist_var.at(af.uncertain_atts[i]), af.arg_exists_var[af.uncertain_atts[i].first] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { -af.source_and_att_exist_var.at(af.uncertain_atts[i]), af.att_exists_var.at(af.uncertain_atts[i]) };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			vector<int> clause = { af.source_and_att_exist_var.at(af.uncertain_atts[i]), -af.arg_exists_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
			solver.add_clause(clause);
		}
	}
}

void add_arg_exists_and_level_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			for (uint32_t n = 1; n < (af.args+1)/2; n++) {
				vector<int> clause = { -af.arg_exists_and_level_var.at(make_pair(i,n)), af.arg_exists_var[i] };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			for (uint32_t n = 1; n < (af.args+1)/2; n++) {
				vector<int> clause = { -af.arg_exists_and_level_var.at(make_pair(i,n)), af.level_var.at(make_pair(i,n)) };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			for (uint32_t n = 1; n < (af.args+1)/2; n++) {
				vector<int> clause = { af.arg_exists_and_level_var.at(make_pair(i,n)), -af.arg_exists_var[i], -af.level_var.at(make_pair(i,n)) };
				solver.add_clause(clause);
			}
		}
	}
}

void add_source_exists_and_level_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			vector<int> clause = { -af.source_exists_and_level_var.at(make_pair(af.uncertain_atts[i],n)), af.level_var.at(make_pair(af.uncertain_atts[i].first, n)) };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			vector<int> clause = { -af.source_exists_and_level_var.at(make_pair(af.uncertain_atts[i],n)), af.att_exists_var.at(af.uncertain_atts[i]) };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
			for (uint32_t n = 1; n < (af.args+1)/2; n++) {
				vector<int> clause = { -af.source_exists_and_level_var.at(make_pair(af.uncertain_atts[i],n)), af.arg_exists_var[af.uncertain_atts[i].first] };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			if (!af.is_uncertain_arg[af.uncertain_atts[i].first]) {
				vector<int> clause = { af.source_exists_and_level_var.at(make_pair(af.uncertain_atts[i],n)), -af.level_var.at(make_pair(af.uncertain_atts[i].first, n)), -af.att_exists_var.at(af.uncertain_atts[i]) };
				solver.add_clause(clause);
			} else {
				vector<int> clause = { af.source_exists_and_level_var.at(make_pair(af.uncertain_atts[i],n)), -af.level_var.at(make_pair(af.uncertain_atts[i].first, n)), -af.arg_exists_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]) };
				solver.add_clause(clause);
			}
		}
	}   
}

void add_arg_rejected_by_level_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.level_var.at(make_pair(af.attackers[i][j], n)), af.arg_rejected_by_level_var.at(make_pair(i, n)) };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.arg_exists_and_level_var.at(make_pair(af.attackers[i][j], n)), af.arg_rejected_by_level_var.at(make_pair(i, n)) };
					solver.add_clause(clause); 
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				vector<int> clause = { -af.source_exists_and_level_var.at(make_pair(make_pair(af.uncertain_attackers[i][j], i), n)), af.arg_rejected_by_level_var.at(make_pair(i, n)) };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			vector<int> clause(af.attackers[i].size() + af.uncertain_attackers[i].size() + 1);
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					clause[j] = af.level_var.at(make_pair(af.attackers[i][j], n));
				} else {
					clause[j] = af.arg_exists_and_level_var.at(make_pair(af.attackers[i][j], n));
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				clause[af.attackers[i].size() + j] = af.source_exists_and_level_var.at(make_pair(make_pair(af.uncertain_attackers[i][j], i), n));
			}
			clause[af.attackers[i].size() + af.uncertain_attackers[i].size()] = -af.arg_rejected_by_level_var.at(make_pair(i, n));
			solver.add_clause(clause);
		}
	}
}

void add_arg_exists_implies_rejected_by_level_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			if (af.is_uncertain_arg[i]) {
				vector<int> clause = { af.arg_exists_var[i], af.arg_exists_implies_rejected_by_level_var.at(make_pair(i, n)) };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			if (af.is_uncertain_arg[i]) {
				vector<int> clause = { -af.arg_rejected_by_level_var.at(make_pair(i, n)), af.arg_exists_implies_rejected_by_level_var.at(make_pair(i, n)) };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			if (af.is_uncertain_arg[i]) {
				vector<int> clause = { -af.arg_exists_var[i], af.arg_rejected_by_level_var.at(make_pair(i,n)), -af.arg_exists_implies_rejected_by_level_var.at(make_pair(i,n)) };
				solver.add_clause(clause);
			}
		}
	}
}

void add_source_rejected_by_level_clauses(const AF & af, SAT_Solver & solver)
{
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			vector<int> clause = { -af.arg_rejected_by_level_var.at(make_pair(af.uncertain_atts[i].first, n)), af.source_rejected_by_level_var.at(make_pair(af.uncertain_atts[i], n)) };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			vector<int> clause = { af.att_exists_var.at(af.uncertain_atts[i]), af.source_rejected_by_level_var.at(make_pair(af.uncertain_atts[i], n)) };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			if (af.is_uncertain_arg[af.uncertain_atts[i].first]) {
				vector<int> clause = { af.arg_exists_var[af.uncertain_atts[i].first], af.source_rejected_by_level_var.at(make_pair(af.uncertain_atts[i], n)) };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.uncertain_atts.size(); i++) {
		for (uint32_t n = 1; n < (af.args+1)/2; n++) {
			if (!af.is_uncertain_arg[af.uncertain_atts[i].first]) {
				vector<int> clause = { af.arg_rejected_by_level_var.at(make_pair(af.uncertain_atts[i].first, n)), -af.att_exists_var.at(af.uncertain_atts[i]), -af.source_rejected_by_level_var.at(make_pair(af.uncertain_atts[i], n)) };
				solver.add_clause(clause);
			} else {
				vector<int> clause = { af.arg_rejected_by_level_var.at(make_pair(af.uncertain_atts[i].first, n)), -af.arg_exists_var[af.uncertain_atts[i].first], -af.att_exists_var.at(af.uncertain_atts[i]), -af.source_rejected_by_level_var.at(make_pair(af.uncertain_atts[i], n)) };
				solver.add_clause(clause);
			}
		}
	}
}

void add_grounded(const AF & af, SAT_Solver & solver)
{
	add_target_clauses(af, solver);
	add_additional_clauses(af, solver);
	add_symmetric_clauses(af, solver);
	for (uint32_t n = 2; n <= (af.args+1)/2; n++) {
		for (uint32_t i = 0; i < af.args; i++) {
			vector<int> clause = { -af.level_var.at(make_pair(i,n-1)), af.level_var.at(make_pair(i,n)) };
			solver.add_clause(clause);
		}
	}
	add_source_and_att_exist_clauses(af, solver);
	add_arg_exists_and_level_clauses(af, solver);
	add_source_exists_and_level_clauses(af, solver);
	add_arg_rejected_by_level_clauses(af, solver);
	add_arg_exists_implies_rejected_by_level_clauses(af, solver);
	add_source_rejected_by_level_clauses(af, solver);
	// level one
	for (uint32_t i = 0; i < af.args; i++) {
		if (af.is_uncertain_arg[i]) {
			vector<int> clause = { -af.level_var.at(make_pair(i,1)), af.arg_exists_var[i] };
			solver.add_clause(clause);
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.attackers[i][j]]) {
				vector<int> clause = { -af.level_var.at(make_pair(i,1)) };
				solver.add_clause(clause);
			} else {
				vector<int> clause = { -af.level_var.at(make_pair(i,1)), -af.arg_exists_var[af.attackers[i][j]] };
				solver.add_clause(clause);
			}
		}
		for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.uncertain_attackers[i][j]]) {
				vector<int> clause = { -af.level_var.at(make_pair(i,1)), -af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
				solver.add_clause(clause);
			} else {
				vector<int> clause = { -af.level_var.at(make_pair(i,1)), -af.source_and_att_exist_var.at(make_pair(af.uncertain_attackers[i][j], i)) };
				solver.add_clause(clause);
			}
		}
	}
	for (uint32_t i = 0; i < af.args; i++) {
		vector<int> clause;
		clause.reserve(af.attackers[i].size() + af.uncertain_attackers[i].size() + 2);
		clause.push_back(af.level_var.at(make_pair(i,1)));
		if (af.is_uncertain_arg[i]) {
			clause.push_back(-af.arg_exists_var[i]);
		}
		bool definite_attack = false;
		for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.attackers[i][j]]) {
				definite_attack = true;
				break;
			} else {
				clause.push_back(af.arg_exists_var[af.attackers[i][j]]);
			}
		}
		if (definite_attack) continue;
		for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
			if (!af.is_uncertain_arg[af.uncertain_attackers[i][j]]) {
				clause.push_back(af.att_exists_var.at(make_pair(af.uncertain_attackers[i][j], i)));
			} else {
				clause.push_back(af.source_and_att_exist_var.at(make_pair(af.uncertain_attackers[i][j], i)));
			}
		}
		solver.add_clause(clause);
	}
	// level n >= 2
	for (uint32_t n = 2; n <= (af.args+1)/2; n++) {
		for (uint32_t i = 0; i < af.args; i++) {
			if (af.is_uncertain_arg[i]) {
				vector<int> clause = { -af.level_var.at(make_pair(i, n)), af.arg_exists_var[i] };
				solver.add_clause(clause);
			}
		}
		for (uint32_t i = 0; i < af.args; i++) {
			for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
				if (!af.is_uncertain_arg[af.attackers[i][j]]) {
					vector<int> clause = { -af.level_var.at(make_pair(i, n)), af.arg_rejected_by_level_var.at(make_pair(af.attackers[i][j], n-1)) };
					solver.add_clause(clause);
				} else {
					vector<int> clause = { -af.level_var.at(make_pair(i, n)), af.arg_exists_implies_rejected_by_level_var.at(make_pair(af.attackers[i][j], n-1)) };
					solver.add_clause(clause);
				}
			}
			for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
				vector<int> clause = { -af.level_var.at(make_pair(i, n)), af.source_rejected_by_level_var.at(make_pair(make_pair(af.uncertain_attackers[i][j], i), n-1)) };
				solver.add_clause(clause);
			}
		}
		for (uint32_t i = 0; i < af.args; i++) {
			if (!af.is_uncertain_arg[i]) {
				vector<int> clause(af.attackers[i].size() + af.uncertain_attackers[i].size() + 1);
				clause[0] = af.level_var.at(make_pair(i, n));
				for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
					if (!af.is_uncertain_arg[af.attackers[i][j]]) {
						clause[j+1] = -af.arg_rejected_by_level_var.at(make_pair(af.attackers[i][j], n-1));
					} else {
						clause[j+1] = -af.arg_exists_implies_rejected_by_level_var.at(make_pair(af.attackers[i][j], n-1));
					}
				}
				for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
					clause[af.attackers[i].size() + j + 1] = -af.source_rejected_by_level_var.at(make_pair(make_pair(af.uncertain_attackers[i][j], i), n-1));
				}
				solver.add_clause(clause);
			} else {
				vector<int> clause(af.attackers[i].size() + af.uncertain_attackers[i].size() + 2);
				clause[0] = af.level_var.at(make_pair(i, n));
				for (uint32_t j = 0; j < af.attackers[i].size(); j++) {
					if (!af.is_uncertain_arg[af.attackers[i][j]]) {
						clause[j+1] = -af.arg_rejected_by_level_var.at(make_pair(af.attackers[i][j], n-1));
					} else {
						clause[j+1] = -af.arg_exists_implies_rejected_by_level_var.at(make_pair(af.attackers[i][j], n-1));
					}
				}
				for (uint32_t j = 0; j < af.uncertain_attackers[i].size(); j++) {
					clause[af.attackers[i].size() + j + 1] = -af.source_rejected_by_level_var.at(make_pair(make_pair(af.uncertain_attackers[i][j], i), n-1));
				}
				clause[af.attackers[i].size() + af.uncertain_attackers[i].size() + 1] = -af.arg_exists_var[i];
				solver.add_clause(clause);
			}
		}
	}
	// accepted <-> on some level
	for (uint32_t i = 0; i < af.args; i++) {
		vector<int> clause((af.args+1)/2+1);
		clause[0] = -af.arg_accepted_var[i];
		for (uint32_t n = 1; n <= (af.args+1)/2; n++) {
			clause[n] = af.level_var.at(make_pair(i, n));
		}
		solver.add_clause(clause);
	}
	for (uint32_t i = 0; i < af.args; i++) {
		for (uint32_t n = 1; n <= (af.args+1)/2; n++) {
			vector<int> clause = { af.arg_accepted_var[i], -af.level_var.at(make_pair(i, n)) };
			solver.add_clause(clause);
		}
	}

}

}