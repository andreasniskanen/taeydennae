/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "AF.h"

#include <iostream>

using namespace std;

void AF::add_argument(string arg)
{
	int_to_arg.push_back(arg);
	arg_to_int[arg] = args++;
	is_uncertain_arg.push_back(false);
	is_control_arg.push_back(false);
}

void AF::add_uncertain_argument(string arg)
{
	int_to_arg.push_back(arg);
	arg_to_int[arg] = args++;
	is_uncertain_arg.push_back(true);
	is_control_arg.push_back(false);
	uncertain_args.push_back(arg_to_int[arg]);
}

void AF::add_control_argument(string arg)
{
	int_to_arg.push_back(arg);
	arg_to_int[arg] = args++;
	is_uncertain_arg.push_back(true);
	uncertain_args.push_back(arg_to_int[arg]);
	is_control_arg.push_back(true);
	control_args.push_back(arg_to_int[arg]);
}

void AF::add_attack(pair<string,string> att)
{
	if (arg_to_int.find(att.first) == arg_to_int.end() || arg_to_int.find(att.second) == arg_to_int.end()) {
		cerr << "Warning: Ignoring attack att(" << att.first << "," << att.second << ").\n";
		return;
	}
	uint32_t source = arg_to_int[att.first];
	uint32_t target = arg_to_int[att.second];
	atts.push_back(make_pair(source, target));
	attackers[target].push_back(source);
}

void AF::add_uncertain_attack(pair<string,string> att)
{
	if (arg_to_int.find(att.first) == arg_to_int.end() || arg_to_int.find(att.second) == arg_to_int.end()) {
		cerr << "Warning: Ignoring attack ?att(" << att.first << "," << att.second << ").\n";
		return;
	}
	uncertain_atts.push_back(make_pair(arg_to_int[att.first], arg_to_int[att.second]));
	uncertain_attackers[arg_to_int[att.second]].push_back(arg_to_int[att.first]);
}

void AF::add_symmetric_attack(pair<string,string> att)
{
	if (arg_to_int.find(att.first) == arg_to_int.end() || arg_to_int.find(att.second) == arg_to_int.end()) {
		cerr << "Warning: Ignoring attack satt(" << att.first << "," << att.second << ").\n";
		return;
	}
	if (att.first == att.second) {
		add_attack(att);
	} else {
		symmetric_atts.push_back(make_pair(arg_to_int[att.first], arg_to_int[att.second]));
		add_uncertain_attack(att);
		add_uncertain_attack(make_pair(att.second, att.first));
	}
}

void AF::set_target(string query, string delim)
{
	size_t pos = 0;
	string arg;
	while ((pos = query.find(delim)) != string::npos) {
		arg = query.substr(0, pos);
		query.erase(0, pos + delim.length());
		if (arg_to_int.find(arg) == arg_to_int.end() || is_uncertain_arg[arg_to_int[arg]]) {
			cerr << "Warning: Ignoring target argument " << arg << "\n";
			continue;
		}
		target_args.push_back(arg_to_int[arg]);
	}
	if (arg_to_int.find(query) == arg_to_int.end() || is_uncertain_arg[arg_to_int[query]]) {
		cerr << "Warning: Ignoring target argument " << query << "\n";
		return;
	}
	target_args.push_back(arg_to_int[query]);
}

void AF::initialize_att_containers()
{
	attackers.resize(args);
	uncertain_attackers.resize(args);
}

void AF::initialize_vars(bool grounded)
{
	arg_accepted_var.resize(args);
	arg_exists_var.resize(args);

	for (uint32_t i = 0; i < args; i++) {
		arg_accepted_var[i] = ++count;
	}

	for (uint32_t i = 0; i < args; i++) {
		if (is_uncertain_arg[i]) {
			arg_exists_var[i] = ++count;
			arg_exists_var_to_arg[count] = i;
		}
	}

	for (uint32_t i = 0; i < uncertain_atts.size(); i++) {
		att_exists_var[uncertain_atts[i]] = ++count;
	}

	if (target_args.size() == 1) {
		target_var = arg_accepted_var[target_args[0]];
	} else {
		target_var = ++count;
	}

	if (!grounded) {

		arg_exists_and_accepted_var.resize(args);
		arg_rejected_var.resize(args);
		arg_exists_implies_rejected_var.resize(args);

		for (uint32_t i = 0; i < args; i++) {
			if (is_uncertain_arg[i]) {
				arg_exists_and_accepted_var[i] = ++count;
			}
		}
		
		for (uint32_t i = 0; i < args; i++) {
			arg_rejected_var[i] = ++count;
		}

		for (uint32_t i = 0; i < args; i++) {
			if (is_uncertain_arg[i]) {
				arg_exists_implies_rejected_var[i] = ++count;
			}
		}
		
		for (uint32_t i = 0; i < uncertain_atts.size(); i++) {
			source_accepted_var[uncertain_atts[i]] = ++count;
		}
		
		for (uint32_t i = 0; i < uncertain_atts.size(); i++) {
			source_rejected_var[uncertain_atts[i]] = ++count;
		}

		selector_var = ++count;

	} else {

		for (uint32_t i = 0; i < args; i++) {
			for (uint32_t n = 1; n <= (args+1)/2; n++) {
				level_var[make_pair(i,n)] = ++count;
			}
		}

		for (uint32_t i = 0; i < uncertain_atts.size(); i++) {
			if (is_uncertain_arg[uncertain_atts[i].first]) {
				source_and_att_exist_var[uncertain_atts[i]] = ++count;
			}
		}

		for (uint32_t i = 0; i < args; i++) {
			if (is_uncertain_arg[i]) {
				for (uint32_t n = 1; n < (args+1)/2; n++) {
					arg_exists_and_level_var[make_pair(i,n)] = ++count;
				}
			}
		}

		for (uint32_t i = 0; i < uncertain_atts.size(); i++) {
			for (uint32_t n = 1; n < (args+1)/2; n++) {
				source_exists_and_level_var[make_pair(uncertain_atts[i], n)] = ++count;
			}
		}

		for (uint32_t i = 0; i < args; i++) {
			for (uint32_t n = 1; n < (args+1)/2; n++) {
				arg_rejected_by_level_var[make_pair(i,n)] = ++count;
			}
		}

		for (uint32_t i = 0; i < args; i++) {
			if (is_uncertain_arg[i]) {
				for (uint32_t n = 1; n < (args+1)/2; n++) {
					arg_exists_implies_rejected_by_level_var[make_pair(i,n)] = ++count;
				}
			}
		}

		for (uint32_t i = 0; i < uncertain_atts.size(); i++) {
			for (uint32_t n = 1; n < (args+1)/2; n++) {
				source_rejected_by_level_var[make_pair(uncertain_atts[i], n)] = ++count;
			}
		}
	
	}
}