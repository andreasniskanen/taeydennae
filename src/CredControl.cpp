/*!
 * Copyright (c) <2020> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"
#include "QDIMACS.h"
#include "Util.h"
#include "CredControl.h"
#include "SkeptControl.h"

#include <iostream>

using namespace std;

namespace CredControl {

bool admissible(AF & af)
{
	if (af.qdimacs_output) {
		QDIMACS::print_cred(af);
		return false;
	}
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_admissible(af, solver);
	uint32_t outer_iters = 0;
	uint32_t inner_iters = 0;
	vector<int> query_arg_accepted = { af.target_var };
	while (true) {
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		outer_iters++;
		if (!pos_cred_accept) {
			cout << "c Outer iterations: " << outer_iters << "\n";
			cout << "c Inner iterations: " << inner_iters << "\n";
			return false;
		}
#if defined(REFINEMENT)
		vector<int> clause = optimized_completion_refinement(af, solver.assignment, AD);
#else
		vector<int> clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(clause);
		while (true) {
			vector<int> control = control_assumptions(af, solver.assignment, -af.target_var);
			bool nec_skept_accept = solver.solve(control);
			inner_iters++;
			if (!nec_skept_accept) {
				cout << "c Outer iterations: " << outer_iters << "\n";
				cout << "c Inner iterations: " << inner_iters << "\n";
				print_control_configuration(af, solver.assignment);
				return true;
			}
			vector<int> completion = completion_assumptions(af, solver.assignment, af.target_var);
			bool cred_accept = solver.solve(completion);
			if (!cred_accept) break;
#if defined(REFINEMENT)
			vector<int> refinement = optimized_completion_refinement(af, solver.assignment, AD);
#else
			vector<int> refinement = trivial_completion_refinement(af, solver.assignment);
#endif
			solver.add_clause(refinement);
		}
#if defined(REFINEMENT)
		vector<int> refinement = core_control_refinement(af, solver.conflict);
#else
		vector<int> refinement = trivial_control_refinement(af, solver.assignment);
#endif
		solver.add_clause(refinement);
	}
}

bool complete(AF & af)
{
	if (af.qdimacs_output) {
		QDIMACS::print_cred(af);
		return false;
	}
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_complete(af, solver);
	uint32_t outer_iters = 0;
	uint32_t inner_iters = 0;
	vector<int> query_arg_accepted = { af.target_var };
	while (true) {
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		outer_iters++;
		if (!pos_cred_accept) {
			cout << "c Outer iterations: " << outer_iters << "\n";
			cout << "c Inner iterations: " << inner_iters << "\n";
			return false;
		}
#if defined(REFINEMENT)
		vector<int> clause = optimized_completion_refinement(af, solver.assignment, AD);
#else
		vector<int> clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(clause);
		while (true) {
			vector<int> control = control_assumptions(af, solver.assignment, -af.target_var);
			bool nec_skept_accept = solver.solve(control);
			inner_iters++;
			if (!nec_skept_accept) {
				cout << "c Outer iterations: " << outer_iters << "\n";
				cout << "c Inner iterations: " << inner_iters << "\n";
				print_control_configuration(af, solver.assignment);
				return true;
			}
			vector<int> completion = completion_assumptions(af, solver.assignment, af.target_var);
			bool cred_accept = solver.solve(completion);
			if (!cred_accept) break;
#if defined(REFINEMENT)
			vector<int> refinement = optimized_completion_refinement(af, solver.assignment, AD);
#else
			vector<int> refinement = trivial_completion_refinement(af, solver.assignment);
#endif
			solver.add_clause(refinement);
		}
#if defined(REFINEMENT)
		vector<int> refinement = core_control_refinement(af, solver.conflict);
#else
		vector<int> refinement = trivial_control_refinement(af, solver.assignment);
#endif
		solver.add_clause(refinement);
	}
}

bool preferred(AF & af)
{
	return complete(af);
}

bool stable(AF & af)
{
	if (af.qdimacs_output) {
		QDIMACS::print_cred(af);
		return false;
	}
	SAT_Solver solver = SAT_Solver(af.count);
	Encodings::add_stable_with_selector(af, solver);
	uint32_t outer_iters = 0;
	uint32_t inner_iters = 0;
	vector<int> query_arg_accepted = { af.selector_var, af.target_var };
	while (true) {
		bool pos_cred_accept = solver.solve(query_arg_accepted);
		outer_iters++;
		if (!pos_cred_accept) {
			cout << "c Outer iterations: " << outer_iters << "\n";
			cout << "c Inner iterations: " << inner_iters << "\n";
			return false;
		}
#if defined(REFINEMENT)
		vector<int> clause = optimized_completion_refinement(af, solver.assignment, ST);
#else
		vector<int> clause = trivial_completion_refinement(af, solver.assignment);
#endif
		solver.add_clause(clause);
		while (true) {
			vector<int> control = control_assumptions(af, solver.assignment, -af.target_var);
			control.push_back(af.selector_var);
			bool nec_skept_accept = solver.solve(control);
			inner_iters++;
			if (!nec_skept_accept) {
				control = control_assumptions(af, solver.assignment, af.target_var);
				control.push_back(af.selector_var);
				while(solver.solve(control)) {
					inner_iters++;
#if defined(REFINEMENT)
					vector<int> blocking_clause = optimized_completion_refinement(af, solver.assignment, ST);
#else
					vector<int> blocking_clause = trivial_completion_refinement(af, solver.assignment);
#endif
					solver.add_clause(blocking_clause);
				}
				vector<int> select = control_assumptions(af, solver.assignment, -af.selector_var);
				bool sat = solver.solve(select);
				if (!sat) {
					cout << "c Outer iterations: " << outer_iters << "\n";
					cout << "c Inner iterations: " << inner_iters << "\n";
					print_control_configuration(af, solver.assignment);
					return true;
				} else {
					vector<int> completion = completion_assumptions(af, solver.assignment, af.selector_var);
					bool exists_extension = solver.solve(completion);
					// assert(!exists_extension)
					break;
				}
			}
			vector<int> completion = completion_assumptions(af, solver.assignment, af.target_var);
			completion.push_back(af.selector_var);
			bool cred_accept = solver.solve(completion);
			if (!cred_accept) break;
#if defined(REFINEMENT)
			vector<int> refinement = optimized_completion_refinement(af, solver.assignment, ST);
#else
			vector<int> refinement = trivial_completion_refinement(af, solver.assignment);
#endif
			solver.add_clause(refinement);
		}
#if defined(REFINEMENT)
		vector<int> refinement = core_control_refinement(af, solver.conflict);
#else
		vector<int> refinement = trivial_control_refinement(af, solver.assignment);
#endif
		solver.add_clause(refinement);
	}
}

bool grounded(AF & af)
{
	return SkeptControl::grounded(af);
}

}