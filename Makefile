SRCDIR    = src
BUILDDIR  = build
TARGET    = bin/taeydennae

SRCEXT    = cpp
ALLSRCS   = $(wildcard $(SRCDIR)/*.$(SRCEXT))
SATSRCS   = $(wildcard $(SRCDIR)/*Solver.$(SRCEXT))
SOURCES   = $(filter-out $(SATSRCS),  $(ALLSRCS))
OBJECTS   = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

CXX       = g++
CFLAGS    = -Wall -Wno-parentheses -std=c++11
COPTIMIZE = -O3
LFLAGS    = -Wall
IFLAGS    = -I include

CFLAGS   += $(COPTIMIZE)
CFLAGS   += -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS

# comment out the following line for trivial CEGAR refinement
CFLAGS   += -D REFINEMENT

# choose glucose-inc to compile Glucose in incremental mode
SAT_SOLVER = glucose
#SAT_SOLVER = glucose-inc

GLUCOSE    = lib/glucose-syrup-4.1

ifeq ($(SAT_SOLVER), glucose)
	CFLAGS  += -D SAT_GLUCOSE
	IFLAGS  += -I $(GLUCOSE)
	LFLAGS  += -lz
	OBJECTS += $(BUILDDIR)/GlucoseSolver.o $(BUILDDIR)/Solver.oc
	SAT_DIR  = $(GLUCOSE)
else ifeq ($(SAT_SOLVER), glucose-inc)
	CFLAGS  += -D SAT_GLUCOSE -D INCREMENTAL
	IFLAGS  += -I $(GLUCOSE)
	LFLAGS  += -lz
	OBJECTS += $(BUILDDIR)/GlucoseSolver.o $(BUILDDIR)/Solver.oc
	SAT_DIR  = $(GLUCOSE)
else
	$(error No SAT solver specified.)
endif

$(TARGET): $(OBJECTS)
	@echo "Linking..."
	@echo "$(CXX) $^ -o $(TARGET) $(LFLAGS)"; $(CXX) $^ -o $(TARGET) $(LFLAGS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@echo "Compiling..."
	@mkdir -p $(BUILDDIR)
	@echo "$(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<"; $(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<

$(BUILDDIR)/%.oc: $(SAT_DIR)/core/%.cc
	@echo "Compiling core..."
	@echo "$(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<"; $(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<

clean:
	@echo "Cleaning..."
	@echo "rm -r $(BUILDDIR) $(TARGET)"; rm -r $(BUILDDIR) $(TARGET)
