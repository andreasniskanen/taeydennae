import os
import sys
import numpy as np

# source folder for base AFs
original_instance_folder = sys.argv[1]
# output folder for control AFs
generated_instance_folder = sys.argv[2]
seed = 20200121

control_probs = [0.05, 0.1, 0.15, 0.2]
uncertain_probs = [0, 0.05, 0.1, 0.15, 0.2]

np.random.seed(seed)

count = 0
for file in sorted(os.listdir(original_instance_folder)):
	count += 1
	if file.endswith(".apx"):
		lines = open(original_instance_folder + "/" + file, "r").read().split("\n")
		args = [line.replace("arg(","").replace(").","") for line in lines if line.startswith("arg")]
		query = open(original_instance_folder + "/" + file + ".arg", "r").read().strip()
		atts = [line.replace("att(","").replace(").","").split(",") for line in lines if line.startswith("att")]
		for p_c in control_probs:
			con_args = []
			is_con_arg = { arg: False for arg in args }
			for arg in args:
				# argument is control argument with probability p_c as long as it is not the query
				if np.random.uniform() < p_c and arg != query:
					con_args += [arg]
					is_con_arg[arg] = True
			for p_u in uncertain_probs:
				def_args = []
				def_atts = []
				inc_args = []
				inc_atts = []
				bid_atts = []
				is_def_att = { ",".join(att) : False for att in atts }
				for s,t in atts:
					is_def_att.update({ ",".join([t,s]) : False })
				is_inc_att = { ",".join(att) : False for att in atts }
				for s,t in atts:
					is_inc_att.update({ ",".join([t,s]) : False })
				is_bid_att = { ",".join(att) : False for att in atts }
				for s,t in atts:
					is_bid_att.update({ ",".join([t,s]) : False })
				for arg in args:
					# argument is uncertain with probability p_u as long as it is not a control argument or the query
					if np.random.uniform() < p_u and not is_con_arg[arg] and arg != query:
						inc_args += [arg]
					# otherwise it is definite as long as it is not a control argument
					elif not is_con_arg[arg]:
						def_args += [arg]
				for att in atts:
					s,t = att
					# if either endpoint is control argument this is a definite attack
					if is_con_arg[s] or is_con_arg[t]:
						def_atts += [att]
					else:
						# you don't want to add an attack if its reverse attack is already bidirectional
						if not is_bid_att[",".join([t,s])]:
							u = np.random.uniform()
							# attack is uncertain with probability p_u, flip a coin whether it is existence or direction
							if u < p_u/2.0:
								inc_atts += [att]
								is_inc_att[",".join(att)] = True
							# if it is direction, make sure that the reverse attack is not already def or inc
							elif u < p_u and not is_def_att[",".join([t,s])] and not is_inc_att[",".join([t,s])]:
								bid_atts += [att]
								is_bid_att[",".join(att)] = True
								is_bid_att[",".join([t,s])] = True
							else:
								def_atts += [att]
								is_def_att[",".join(att)] = True
				filename = file.replace(".apx","") + "_" + str(int(100*p_c)) + "_" + str(int(100*p_u)) + ".apx"
				out = open(generated_instance_folder + "/" + filename, "w")
				for arg in def_args:
					out.write("arg(" + arg + ").\n")
				for arg in con_args:
					out.write("carg(" + arg + ").\n")
				for arg in inc_args:
					out.write("?arg(" + arg + ").\n")
				for s,t in def_atts:
					out.write("att(" + s + "," + t + ").\n")
				for s,t in inc_atts:
					out.write("?att(" + s + "," + t + ").\n")
				for s,t in bid_atts:
					out.write("batt(" + s + "," + t + ").\n")
				out.close()
				qout = open(generated_instance_folder + "/" + filename + ".arg", "w")
				qout.write(query)
				qout.close()
