#!/bin/bash

echo "Downloading ICCMA 2019 instance set..."
if [ -f "iccma-instances.tar.gz" ]
then
	echo "Already exists!"
else
	wget http://argumentationcompetition.org/2019/iccma-instances.tar.gz
fi
echo "Extracting archive..."
tar xvzf iccma-instances.tar.gz

echo "Generating control AFs (this will take a while)..."
mkdir -p instances-control
python3 generate_caf_instances.py instances instances-control
echo "Done!"
rm iccma-instances.tar.gz
rm -rf instances