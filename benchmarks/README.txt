WARNING: The total size of the instances will be very large. Make sure you have at least 50 GB
of free space on your machine.

Incomplete AFs
==============

To generate benchmark instances, run the script ./download_and_run_iaf_generator.sh in this folder.
It will automatically download instance sets A and B from ICCMA 2017, and generate incomplete AFs
based on the AFs in these instance sets. For details, see reference below.

The CSV files runtime_data_first_level.csv and runtime_data_second_level.csv contain the runtime
data for these benchmarks. Again, for details on the benchmark setup, see reference below.

Deciding Acceptance in Incomplete Argumentation Frameworks.
Andreas Niskanen, Daniel Neugebauer, Matti Järvisalo, Jörg Rothe.
Proc. AAAI 2020.

Control AFs
===========

To generate benchmark instances, run the script ./download_and_run_caf_generator.sh in this folder.
It will automatically download the ICCMA 2019 instance set, and generate control AFs
based on the AFs in these instance sets. For details, see reference below.

Controllability of Control Argumentation Frameworks.
Andreas Niskanen, Daniel Neugebauer, Matti Järvisalo.
Proc. IJCAI 2020.